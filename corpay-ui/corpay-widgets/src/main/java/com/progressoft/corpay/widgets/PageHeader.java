package com.progressoft.corpay.widgets;

import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLElement;
import org.dominokit.domino.api.shared.extension.Content;
import org.dominokit.domino.ui.column.Column;
import org.dominokit.domino.ui.header.BlockHeader;
import org.dominokit.domino.ui.row.Row;
import org.dominokit.domino.ui.style.Style;
import org.dominokit.domino.ui.style.Styles;
import org.dominokit.domino.ui.utils.ElementUtil;
import org.jboss.gwt.elemento.core.IsElement;

import static org.jboss.gwt.elemento.core.Elements.div;

public class PageHeader implements IsElement<HTMLDivElement>, Content<HTMLDivElement> {

    private final Row row = Style.of(Row.create())
            .setMarginRight("0px").get();
    private final Column column = Column.create()
            .onLarge(Column.OnLarge.six)
            .onMedium(Column.OnMedium.six)
            .onSmall(Column.OnSmall.twelve)
            .onXSmall(Column.OnXSmall.twelve);

    private final HTMLDivElement actionsContainer = div().asElement();
    private final HTMLDivElement primaryContainer = div().css(Styles.clearfix, Styles.pull_right).asElement();
    private final HTMLDivElement secondaryContainer = div().css(Styles.clearfix, Styles.pull_right).asElement();
    private final BlockHeader blockHeader;

    public PageHeader(String title, String description) {
        blockHeader = Style.of(BlockHeader.create(title, description))
                .setMarginBottom("0px")
                .get();
        initPageHeader();
    }

    private void initPageHeader() {
        Column headerColumn = this.column.copy()
                .onLarge(Column.OnLarge.three)
                .onMedium(Column.OnMedium.three)
                .onSmall(Column.OnSmall.twelve)
                .onXSmall(Column.OnXSmall.twelve);

        Column actionsColumn = this.column.copy()
                .onLarge(Column.OnLarge.nine)
                .onMedium(Column.OnMedium.nine)
                .onSmall(Column.OnSmall.twelve)
                .onXSmall(Column.OnXSmall.twelve);

        Column primary = this.column.copy()
                .onLarge(Column.OnLarge.two)
                .onMedium(Column.OnMedium.two)
                .onSmall(Column.OnSmall.two)
                .onXSmall(Column.OnXSmall.six);

        Column secondary = this.column.copy()
                .onLarge(Column.OnLarge.ten)
                .onMedium(Column.OnMedium.ten)
                .onSmall(Column.OnSmall.ten)
                .onXSmall(Column.OnXSmall.six);

        actionsContainer.appendChild(Row.create()
                .addColumn(secondary.addElement(secondaryContainer))
                .addColumn(primary.addElement(primaryContainer))
                .asElement());

        row
                .addColumn(headerColumn.addElement(blockHeader))
                .addColumn(actionsColumn.addElement(actionsContainer))
                .asElement();
    }

    public PageHeader addSecondaryAction(HTMLElement secondaryAction) {
        secondaryContainer.appendChild(secondaryAction);
        return this;
    }

    public PageHeader setPrimaryAction(HTMLElement primaryAction) {
        ElementUtil.clear(primaryContainer);
        primaryContainer.appendChild(primaryAction);
        return this;
    }

    @Override
    public HTMLDivElement asElement() {
        return row.asElement();
    }

    @Override
    public HTMLDivElement get() {
        return asElement();
    }
}
