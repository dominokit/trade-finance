package com.progressoft.corpay.config.client.presenters;

import com.progressoft.corpay.config.client.requests.TradeFinanceConfigurationRequestsFactory;
import com.progressoft.corpay.config.client.views.TradeFinanceConfigurationView;
import com.progressoft.corpay.config.shared.response.TradeFinanceConfigurationResponse;
import com.progressoft.corpay.layout.shared.extension.AppLayoutContext;
import com.progressoft.corpay.layout.shared.extension.AppLayoutExtensionPoint;
import org.dominokit.domino.api.client.annotations.InjectContext;
import org.dominokit.domino.api.client.annotations.Presenter;
import org.dominokit.domino.api.client.mvp.presenter.BaseClientPresenter;
import org.dominokit.domino.api.shared.request.FailedResponseBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Presenter
public class TradeFinanceConfigurationPresenter extends BaseClientPresenter<TradeFinanceConfigurationView> {

    private static final Logger LOGGER = LoggerFactory.getLogger(TradeFinanceConfigurationPresenter.class);
    private AppLayoutContext layoutContext;

    @InjectContext(extensionPoint = AppLayoutExtensionPoint.class)
    public void contributeToLayoutModule(AppLayoutContext context) {
        this.layoutContext = context;
        TradeFinanceConfigurationRequestsFactory.INSTANCE
                .getConfigurations()
                .onSuccess(this::responseRecieved)
                .onFailed(this::onFailed)
                .send();
    }

    private void onFailed(FailedResponseBean failedResponse) {
        LOGGER.error("Failed to load configuration {0}, will use default values",
                failedResponse.getError().getMessage(),
                failedResponse.getError());
        setTitle("Trade Finance");
    }

    private void responseRecieved(TradeFinanceConfigurationResponse response) {
        setTitle(response.getLayoutConfig().getAppTitle());
    }

    private void setTitle(String appTitle) {
        layoutContext.getLayout().setTitle(appTitle);
    }
}