package com.progressoft.corpay.config.client.requests;

import org.dominokit.domino.api.client.annotations.Path;
import org.dominokit.domino.api.client.annotations.RequestFactory;
import org.dominokit.domino.api.client.request.Response;
import com.progressoft.corpay.config.shared.response.TradeFinanceConfigurationResponse;
import com.progressoft.corpay.config.shared.request.TradeFinanceConfigurationRequest;

@RequestFactory
public interface TradeFinanceConfigurationRequests {
    @Path("TradeFinanceConfigurationRequest")
    Response<TradeFinanceConfigurationResponse> getConfigurations();
}
