package com.progressoft.corpay.config.client.views;

import com.progressoft.corpay.config.client.presenters.TradeFinanceConfigurationPresenter;
import org.dominokit.domino.api.client.annotations.UiView;

@UiView(presentable = TradeFinanceConfigurationPresenter.class)
public class DefaultTradeConfigurationView implements TradeFinanceConfigurationView {
}
