package com.progressoft.corpay.config.client;

import com.progressoft.corpay.config.client.contributions.TradeFinanceConfigurationPresenterContributionToAppLayoutExtensionPoint;
import com.progressoft.corpay.config.client.presenters.TradeFinanceConfigurationPresenter;
import com.progressoft.corpay.config.client.presenters.TradeFinanceConfigurationPresenterCommand;
import com.progressoft.corpay.config.client.requests.TradeFinanceConfigurationRequestsFactory;
import com.progressoft.corpay.config.client.requests.TradeFinanceConfigurationRequests_getConfigurationsSender;
import com.progressoft.corpay.config.client.views.DefaultTradeConfigurationView;
import com.progressoft.corpay.layout.shared.extension.AppLayoutExtensionPoint;
import javax.annotation.Generated;
import org.dominokit.domino.api.client.ModuleConfiguration;
import org.dominokit.domino.api.client.extension.ContributionsRegistry;
import org.dominokit.domino.api.client.mvp.PresenterRegistry;
import org.dominokit.domino.api.client.mvp.ViewRegistry;
import org.dominokit.domino.api.client.mvp.presenter.LazyPresenterLoader;
import org.dominokit.domino.api.client.mvp.presenter.Presentable;
import org.dominokit.domino.api.client.mvp.view.LazyViewLoader;
import org.dominokit.domino.api.client.mvp.view.View;
import org.dominokit.domino.api.client.request.CommandRegistry;
import org.dominokit.domino.api.client.request.LazyRequestRestSenderLoader;
import org.dominokit.domino.api.client.request.RequestRestSender;
import org.dominokit.domino.api.client.request.RequestRestSendersRegistry;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.module.client.ClientModuleAnnotationProcessor")
public class TradeFinanceConfigurationModuleConfiguration implements ModuleConfiguration {
  @Override
  public void registerPresenters(PresenterRegistry registry) {
    registry.registerPresenter(new LazyPresenterLoader(TradeFinanceConfigurationPresenter.class.getCanonicalName(), TradeFinanceConfigurationPresenter.class.getCanonicalName()) {
      @Override
      protected Presentable make() {
        return new TradeFinanceConfigurationPresenter();
      }
    });
  }

  @Override
  public void registerViews(ViewRegistry registry) {
    registry.registerView(new LazyViewLoader(TradeFinanceConfigurationPresenter.class.getCanonicalName()) {
      @Override
      protected View make() {
        return new DefaultTradeConfigurationView();
      }
    });
  }

  @Override
  public void registerRequests(CommandRegistry registry) {
    registry.registerCommand(TradeFinanceConfigurationPresenterCommand.class.getCanonicalName(), TradeFinanceConfigurationPresenter.class.getCanonicalName());
  }

  @Override
  public void registerContributions(ContributionsRegistry registry) {
    registry.registerContribution(AppLayoutExtensionPoint.class, new TradeFinanceConfigurationPresenterContributionToAppLayoutExtensionPoint());
  }

  @Override
  public void registerRequestRestSenders(RequestRestSendersRegistry registry) {
    registry.registerRequestRestSender(TradeFinanceConfigurationRequestsFactory.TradeFinanceConfigurationRequests_getConfigurations.class.getCanonicalName(), new LazyRequestRestSenderLoader() {
      @Override
      protected RequestRestSender make() {
        return new TradeFinanceConfigurationRequests_getConfigurationsSender();
      }
    });
  }
}
