package com.progressoft.corpay.config.client.contributions;

import com.progressoft.corpay.config.client.presenters.TradeFinanceConfigurationPresenterCommand;
import com.progressoft.corpay.layout.shared.extension.AppLayoutExtensionPoint;
import javax.annotation.Generated;
import org.dominokit.domino.api.client.annotations.Contribute;
import org.dominokit.domino.api.shared.extension.Contribution;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.inject.InjectContextProcessor")
@Contribute
public class TradeFinanceConfigurationPresenterContributionToAppLayoutExtensionPoint implements Contribution<AppLayoutExtensionPoint> {
  @Override
  public void contribute(AppLayoutExtensionPoint extensionPoint) {
    new TradeFinanceConfigurationPresenterCommand().onPresenterReady(presenter -> presenter.contributeToLayoutModule(extensionPoint.context())).send();
  }
}
