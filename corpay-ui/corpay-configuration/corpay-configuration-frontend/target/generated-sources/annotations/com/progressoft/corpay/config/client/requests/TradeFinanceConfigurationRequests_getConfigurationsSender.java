package com.progressoft.corpay.config.client.requests;

import com.google.gwt.core.client.GWT;
import com.progressoft.corpay.config.shared.response.TradeFinanceConfigurationResponse;
import java.util.Map;
import javax.annotation.Generated;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.dominokit.domino.api.client.ServiceRootMatcher;
import org.dominokit.domino.api.client.annotations.RequestSender;
import org.dominokit.domino.api.client.request.RequestRestSender;
import org.dominokit.domino.api.client.request.ServerRequestCallBack;
import org.dominokit.domino.api.shared.request.VoidRequest;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.Resource;
import org.fusesource.restygwt.client.RestService;
import org.fusesource.restygwt.client.RestServiceProxy;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.handlers.RequestPathProcessor")
@RequestSender(TradeFinanceConfigurationRequestsFactory.TradeFinanceConfigurationRequests_getConfigurations.class)
public class TradeFinanceConfigurationRequests_getConfigurationsSender implements RequestRestSender<VoidRequest> {
  public static final String PATH = "TradeFinanceConfigurationRequest";

  private TradeFinanceConfigurationRequests_getConfigurationsService service = GWT.create(TradeFinanceConfigurationRequests_getConfigurationsService.class);

  @Override
  public void send(VoidRequest request, Map<String, String> headers,
      ServerRequestCallBack callBack) {
    ((RestServiceProxy)service).setResource(new Resource(ServiceRootMatcher.matchedServiceRoot(PATH), headers));
    service.send(request, new MethodCallback<TradeFinanceConfigurationResponse>() {
      @Override
      public void onFailure(Method method, Throwable throwable) {
        callBack.onFailure(throwable);
      }

      @Override
      public void onSuccess(Method method, TradeFinanceConfigurationResponse response) {
        callBack.onSuccess(response);
      }
    });
  }

  public interface TradeFinanceConfigurationRequests_getConfigurationsService extends RestService {
    @POST
    @Path(PATH)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    void send(VoidRequest request, MethodCallback<TradeFinanceConfigurationResponse> callback);
  }
}
