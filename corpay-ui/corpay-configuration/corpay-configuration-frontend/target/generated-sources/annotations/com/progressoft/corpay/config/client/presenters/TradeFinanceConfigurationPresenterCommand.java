package com.progressoft.corpay.config.client.presenters;

import javax.annotation.Generated;
import org.dominokit.domino.api.client.annotations.Command;
import org.dominokit.domino.api.client.request.PresenterCommand;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.module.client.presenters.PresenterCommandProcessor")
@Command
public class TradeFinanceConfigurationPresenterCommand extends PresenterCommand<TradeFinanceConfigurationPresenter> {
}
