package com.progressoft.corpay.config.client.requests;

import com.progressoft.corpay.config.shared.response.TradeFinanceConfigurationResponse;
import javax.annotation.Generated;
import org.dominokit.domino.api.client.annotations.Path;
import org.dominokit.domino.api.client.annotations.Request;
import org.dominokit.domino.api.client.request.Response;
import org.dominokit.domino.api.client.request.ServerRequest;
import org.dominokit.domino.api.shared.request.RequestBean;
import org.dominokit.domino.api.shared.request.VoidRequest;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.group.RequestFactoryProcessor")
public class TradeFinanceConfigurationRequestsFactory implements TradeFinanceConfigurationRequests {
  public static final TradeFinanceConfigurationRequestsFactory INSTANCE = new TradeFinanceConfigurationRequestsFactory();

  @Override
  public Response<TradeFinanceConfigurationResponse> getConfigurations() {
    return new TradeFinanceConfigurationRequests_getConfigurations(RequestBean.VOID_REQUEST);
  }

  @Request
  @Path("TradeFinanceConfigurationRequest")
  public class TradeFinanceConfigurationRequests_getConfigurations extends ServerRequest<VoidRequest, TradeFinanceConfigurationResponse> {
    TradeFinanceConfigurationRequests_getConfigurations(VoidRequest request) {
      super(request);
    }
  }
}
