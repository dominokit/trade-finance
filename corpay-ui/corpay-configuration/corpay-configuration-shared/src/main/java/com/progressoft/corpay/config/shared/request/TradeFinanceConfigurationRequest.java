package com.progressoft.corpay.config.shared.request;

import org.dominokit.domino.api.shared.request.RequestBean;

public class TradeFinanceConfigurationRequest implements RequestBean {

    private String message;

    public TradeFinanceConfigurationRequest() {
    }

    public TradeFinanceConfigurationRequest(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
