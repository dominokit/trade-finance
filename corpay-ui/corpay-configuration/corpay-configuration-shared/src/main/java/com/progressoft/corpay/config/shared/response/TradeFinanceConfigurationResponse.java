package com.progressoft.corpay.config.shared.response;

import com.progressoft.corpay.config.shared.LayoutConfig;
import org.dominokit.domino.api.shared.request.ResponseBean;

public class TradeFinanceConfigurationResponse implements ResponseBean {

    private LayoutConfig layoutConfig;

    public TradeFinanceConfigurationResponse() {
    }

    public TradeFinanceConfigurationResponse(LayoutConfig layoutConfig) {
        this.layoutConfig = layoutConfig;
    }

    public LayoutConfig getLayoutConfig() {
        return layoutConfig;
    }

    public void setLayoutConfig(LayoutConfig layoutConfig) {
        this.layoutConfig = layoutConfig;
    }
}
