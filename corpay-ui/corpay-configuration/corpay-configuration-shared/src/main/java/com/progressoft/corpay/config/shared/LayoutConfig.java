package com.progressoft.corpay.config.shared;

public class LayoutConfig {

    public static final String APP_TITLE = "app.title";
    public static final String MENU_TITLE = "menu.title";
    public static final String DEFAULT_APP_TITLE = "APP";
    public static final String DEFAULT_MENU_TITLE = "MENU";
    
    public static final LayoutConfig DEFAULT = new LayoutConfig(DEFAULT_APP_TITLE, DEFAULT_MENU_TITLE);

    private String appTitle;
    private String menuTitle;

    public LayoutConfig() {
    }

    public LayoutConfig(String appTitle, String menuTitle) {
        this.appTitle = appTitle;
        this.menuTitle = menuTitle;
    }

    public String getAppTitle() {
        return appTitle;
    }

    public void setAppTitle(String appTitle) {
        this.appTitle = appTitle;
    }

    public String getMenuTitle() {
        return menuTitle;
    }

    public void setMenuTitle(String menuTitle) {
        this.menuTitle = menuTitle;
    }
}
