package com.progressoft.corpay.config.shared.extension;

import org.dominokit.domino.api.shared.extension.ExtensionPoint;

public interface TradeFinanceConfigurationExtensionPoint extends ExtensionPoint<TradeFinanceConfigurationContext>{
}
