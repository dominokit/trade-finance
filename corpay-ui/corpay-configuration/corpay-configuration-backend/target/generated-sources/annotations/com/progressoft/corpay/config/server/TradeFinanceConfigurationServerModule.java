package com.progressoft.corpay.config.server;

import com.google.auto.service.AutoService;
import com.progressoft.corpay.config.server.handlers.TradeFinanceConfigurationHandler;
import com.progressoft.corpay.config.server.handlers.TradeFinanceConfigurationHandlerEndpointHandler;
import java.util.function.Supplier;
import javax.annotation.Generated;
import org.dominokit.domino.api.server.config.ServerModuleConfiguration;
import org.dominokit.domino.api.server.endpoint.EndpointsRegistry;
import org.dominokit.domino.api.server.handler.HandlerRegistry;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.server.ServerModuleAnnotationProcessor")
@AutoService(ServerModuleConfiguration.class)
public class TradeFinanceConfigurationServerModule implements ServerModuleConfiguration {
  @Override
  public void registerHandlers(HandlerRegistry registry) {
    registry.registerHandler("TradeFinanceConfigurationRequest",new TradeFinanceConfigurationHandler());
  }

  @Override
  public void registerEndpoints(EndpointsRegistry registry) {
    registry.registerEndpoint("TradeFinanceConfigurationRequest", new Supplier<TradeFinanceConfigurationHandlerEndpointHandler>() {
      @Override
      public TradeFinanceConfigurationHandlerEndpointHandler get() {
        return new TradeFinanceConfigurationHandlerEndpointHandler();
      }
    });
  }
}
