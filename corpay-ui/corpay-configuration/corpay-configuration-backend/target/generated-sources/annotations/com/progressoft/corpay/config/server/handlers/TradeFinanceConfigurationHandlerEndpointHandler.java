package com.progressoft.corpay.config.server.handlers;

import com.progressoft.corpay.config.shared.response.TradeFinanceConfigurationResponse;
import javax.annotation.Generated;
import org.dominokit.domino.api.server.endpoint.AbstractEndpoint;
import org.dominokit.domino.api.shared.request.VoidRequest;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.server.EndpointsProcessor")
public class TradeFinanceConfigurationHandlerEndpointHandler extends AbstractEndpoint<VoidRequest, TradeFinanceConfigurationResponse> {
  @Override
  protected VoidRequest makeNewRequest() {
    return new VoidRequest();
  }

  @Override
  protected Class<VoidRequest> getRequestClass() {
    return VoidRequest.class;
  }
}
