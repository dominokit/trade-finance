package com.progressoft.corpay.config.server;

import com.google.auto.service.AutoService;
import com.progressoft.corpay.config.shared.LayoutConfig;
import org.dominokit.domino.api.server.entrypoint.ServerAppEntryPoint;
import org.dominokit.domino.api.server.entrypoint.VertxContext;

import static com.progressoft.corpay.config.shared.LayoutConfig.APP_TITLE;
import static com.progressoft.corpay.config.shared.LayoutConfig.DEFAULT_APP_TITLE;
import static com.progressoft.corpay.config.shared.LayoutConfig.DEFAULT_MENU_TITLE;
import static com.progressoft.corpay.config.shared.LayoutConfig.MENU_TITLE;

@AutoService(ServerAppEntryPoint.class)
public class ConfigurationEntryPoint implements ServerAppEntryPoint<VertxContext> {

    @Override
    public void onModulesLoaded(VertxContext vertxContext) {
        String appTitle = vertxContext.config().getString(APP_TITLE, DEFAULT_APP_TITLE);
        String menuTitle = vertxContext.config().getString(MENU_TITLE, DEFAULT_MENU_TITLE);
        ConfigurationContext.setLayoutConfig(new LayoutConfig(appTitle, menuTitle));
    }
}
