package com.progressoft.corpay.config.server.handlers;

import com.progressoft.corpay.config.server.ConfigurationContext;
import com.progressoft.corpay.config.shared.response.TradeFinanceConfigurationResponse;
import org.dominokit.domino.api.server.context.ExecutionContext;
import org.dominokit.domino.api.server.handler.Handler;
import org.dominokit.domino.api.server.handler.RequestHandler;
import org.dominokit.domino.api.shared.request.VoidRequest;

import java.util.logging.Logger;

@Handler("TradeFinanceConfigurationRequest")
public class TradeFinanceConfigurationHandler implements RequestHandler<VoidRequest,
        TradeFinanceConfigurationResponse> {
    private static final Logger LOGGER = Logger.getLogger(TradeFinanceConfigurationHandler.class.getName());

    @Override
    public void handleRequest(ExecutionContext<VoidRequest, TradeFinanceConfigurationResponse> executionContext) {
        executionContext.end(new TradeFinanceConfigurationResponse(ConfigurationContext.getLayoutConfig()));
    }
}
