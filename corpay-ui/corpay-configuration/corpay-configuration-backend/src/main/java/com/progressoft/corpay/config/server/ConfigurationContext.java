package com.progressoft.corpay.config.server;

import com.progressoft.corpay.config.shared.LayoutConfig;

public class ConfigurationContext {

    private static LayoutConfig layoutConfig = LayoutConfig.DEFAULT;

    public static LayoutConfig getLayoutConfig() {
        return layoutConfig;
    }

    public static void setLayoutConfig(LayoutConfig layoutConfig) {
        ConfigurationContext.layoutConfig = layoutConfig;
    }
}
