package com.progressoft.corpay.layout.client.contributions;

import com.progressoft.corpay.layout.client.presenters.AppLayoutPresenterCommand;
import javax.annotation.Generated;
import org.dominokit.domino.api.client.annotations.Contribute;
import org.dominokit.domino.api.shared.extension.Contribution;
import org.dominokit.domino.api.shared.extension.MainExtensionPoint;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.inject.InjectContextProcessor")
@Contribute
public class AppLayoutPresenterContributionToMainExtensionPoint implements Contribution<MainExtensionPoint> {
  @Override
  public void contribute(MainExtensionPoint extensionPoint) {
    new AppLayoutPresenterCommand().onPresenterReady(presenter -> presenter.contributeToMainModule(extensionPoint.context())).send();
  }
}
