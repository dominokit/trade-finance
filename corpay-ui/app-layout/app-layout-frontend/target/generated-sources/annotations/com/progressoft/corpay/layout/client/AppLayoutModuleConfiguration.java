package com.progressoft.corpay.layout.client;

import com.progressoft.corpay.layout.client.contributions.AppLayoutPresenterContributionToMainExtensionPoint;
import com.progressoft.corpay.layout.client.presenters.AppLayoutPresenter;
import com.progressoft.corpay.layout.client.presenters.AppLayoutPresenterCommand;
import com.progressoft.corpay.layout.client.requests.AppLayoutRequestsFactory;
import com.progressoft.corpay.layout.client.requests.AppLayoutRequests_requestSender;
import javax.annotation.Generated;
import org.dominokit.domino.api.client.ModuleConfiguration;
import org.dominokit.domino.api.client.extension.ContributionsRegistry;
import org.dominokit.domino.api.client.mvp.PresenterRegistry;
import org.dominokit.domino.api.client.mvp.presenter.LazyPresenterLoader;
import org.dominokit.domino.api.client.mvp.presenter.Presentable;
import org.dominokit.domino.api.client.request.CommandRegistry;
import org.dominokit.domino.api.client.request.LazyRequestRestSenderLoader;
import org.dominokit.domino.api.client.request.RequestRestSender;
import org.dominokit.domino.api.client.request.RequestRestSendersRegistry;
import org.dominokit.domino.api.shared.extension.MainExtensionPoint;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.module.client.ClientModuleAnnotationProcessor")
public class AppLayoutModuleConfiguration implements ModuleConfiguration {
  @Override
  public void registerPresenters(PresenterRegistry registry) {
    registry.registerPresenter(new LazyPresenterLoader(AppLayoutPresenter.class.getCanonicalName(), AppLayoutPresenter.class.getCanonicalName()) {
      @Override
      protected Presentable make() {
        return new AppLayoutPresenter();
      }
    });
  }

  @Override
  public void registerRequests(CommandRegistry registry) {
    registry.registerCommand(AppLayoutPresenterCommand.class.getCanonicalName(), AppLayoutPresenter.class.getCanonicalName());
  }

  @Override
  public void registerContributions(ContributionsRegistry registry) {
    registry.registerContribution(MainExtensionPoint.class, new AppLayoutPresenterContributionToMainExtensionPoint());
  }

  @Override
  public void registerRequestRestSenders(RequestRestSendersRegistry registry) {
    registry.registerRequestRestSender(AppLayoutRequestsFactory.AppLayoutRequests_request.class.getCanonicalName(), new LazyRequestRestSenderLoader() {
      @Override
      protected RequestRestSender make() {
        return new AppLayoutRequests_requestSender();
      }
    });
  }
}
