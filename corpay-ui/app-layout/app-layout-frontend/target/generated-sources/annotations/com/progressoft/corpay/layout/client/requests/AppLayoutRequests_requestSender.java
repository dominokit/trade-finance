package com.progressoft.corpay.layout.client.requests;

import com.google.gwt.core.client.GWT;
import com.progressoft.corpay.layout.shared.request.AppLayoutRequest;
import com.progressoft.corpay.layout.shared.response.AppLayoutResponse;
import java.util.Map;
import javax.annotation.Generated;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.dominokit.domino.api.client.ServiceRootMatcher;
import org.dominokit.domino.api.client.annotations.RequestSender;
import org.dominokit.domino.api.client.request.RequestRestSender;
import org.dominokit.domino.api.client.request.ServerRequestCallBack;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.Resource;
import org.fusesource.restygwt.client.RestService;
import org.fusesource.restygwt.client.RestServiceProxy;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.handlers.RequestPathProcessor")
@RequestSender(AppLayoutRequestsFactory.AppLayoutRequests_request.class)
public class AppLayoutRequests_requestSender implements RequestRestSender<AppLayoutRequest> {
  public static final String PATH = "AppLayoutRequest";

  private AppLayoutRequests_requestService service = GWT.create(AppLayoutRequests_requestService.class);

  @Override
  public void send(AppLayoutRequest request, Map<String, String> headers,
      ServerRequestCallBack callBack) {
    ((RestServiceProxy)service).setResource(new Resource(ServiceRootMatcher.matchedServiceRoot(PATH), headers));
    service.send(request, new MethodCallback<AppLayoutResponse>() {
      @Override
      public void onFailure(Method method, Throwable throwable) {
        callBack.onFailure(throwable);
      }

      @Override
      public void onSuccess(Method method, AppLayoutResponse response) {
        callBack.onSuccess(response);
      }
    });
  }

  public interface AppLayoutRequests_requestService extends RestService {
    @POST
    @Path(PATH)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    void send(AppLayoutRequest request, MethodCallback<AppLayoutResponse> callback);
  }
}
