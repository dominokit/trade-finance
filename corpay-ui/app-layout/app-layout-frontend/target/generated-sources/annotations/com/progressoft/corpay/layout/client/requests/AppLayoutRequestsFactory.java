package com.progressoft.corpay.layout.client.requests;

import com.progressoft.corpay.layout.shared.request.AppLayoutRequest;
import com.progressoft.corpay.layout.shared.response.AppLayoutResponse;
import javax.annotation.Generated;
import org.dominokit.domino.api.client.annotations.Path;
import org.dominokit.domino.api.client.annotations.Request;
import org.dominokit.domino.api.client.request.Response;
import org.dominokit.domino.api.client.request.ServerRequest;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.group.RequestFactoryProcessor")
public class AppLayoutRequestsFactory implements AppLayoutRequests {
  public static final AppLayoutRequestsFactory INSTANCE = new AppLayoutRequestsFactory();

  @Override
  public Response<AppLayoutResponse> request(AppLayoutRequest request) {
    return new AppLayoutRequests_request(request);
  }

  @Request
  @Path("AppLayoutRequest")
  public class AppLayoutRequests_request extends ServerRequest<AppLayoutRequest, AppLayoutResponse> {
    AppLayoutRequests_request(AppLayoutRequest request) {
      super(request);
    }
  }
}
