package com.progressoft.corpay.layout.client.views;

import com.progressoft.corpay.layout.shared.IsLayout;
import org.dominokit.domino.api.client.mvp.view.View;

public interface AppLayoutView extends View {
    IsLayout getLayout();

    void show();
}