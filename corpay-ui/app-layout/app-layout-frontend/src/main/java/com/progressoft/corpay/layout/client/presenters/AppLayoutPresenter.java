package com.progressoft.corpay.layout.client.presenters;

import com.progressoft.corpay.layout.client.views.AppLayoutView;
import com.progressoft.corpay.layout.shared.IsLayout;
import com.progressoft.corpay.layout.shared.extension.AppLayoutContext;
import com.progressoft.corpay.layout.shared.extension.AppLayoutExtensionPoint;
import org.dominokit.domino.api.client.annotations.InjectContext;
import org.dominokit.domino.api.client.annotations.Presenter;
import org.dominokit.domino.api.client.mvp.presenter.BaseClientPresenter;
import org.dominokit.domino.api.shared.extension.MainContext;
import org.dominokit.domino.api.shared.extension.MainExtensionPoint;

@Presenter
public class AppLayoutPresenter extends BaseClientPresenter<AppLayoutView> implements AppLayoutContext {

    @InjectContext(extensionPoint = MainExtensionPoint.class)
    public void contributeToMainModule(MainContext context) {
        view.show();
        applyContributions(AppLayoutExtensionPoint.class, () -> AppLayoutPresenter.this);
    }

    @Override
    public IsLayout getLayout() {
        return view.getLayout();
    }
}