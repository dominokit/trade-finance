package com.progressoft.corpay.layout.client.requests;

import org.dominokit.domino.api.client.annotations.Path;
import org.dominokit.domino.api.client.annotations.RequestFactory;
import org.dominokit.domino.api.client.request.Response;
import com.progressoft.corpay.layout.shared.response.AppLayoutResponse;
import com.progressoft.corpay.layout.shared.request.AppLayoutRequest;

@RequestFactory
public interface AppLayoutRequests {
    @Path("AppLayoutRequest")
    Response<AppLayoutResponse> request(AppLayoutRequest request);
}
