package com.progressoft.corpay.layout.shared;

import org.dominokit.domino.api.shared.extension.Content;

public interface IsLayout {
    IsLayout show();

    IsLayout show(String theme);

    void toggleRightPanel();

    IsLayout showRightPanel();

    IsLayout hideRightPanel();

    void toggleLeftPanel();

    IsLayout showLeftPanel();

    IsLayout hideLeftPanel();

    Content getRightPanel();

    Content getLeftPanel();

    Content getContentPanel();

    Content getTopBar();

    IsNavigationBar getProductNavigation();

    Content getContentSection();

    IsLayout setTitle(String title);

    Content addActionItem(String iconName);

    void showPageHeader();

    void hidePageHeader();

    void setPageHeaderContent(Content content);

    Content getPageHeaderContainer();

    IsLayout fixLeftPanelPosition();

    IsLayout unfixLeftPanelPosition();
}
