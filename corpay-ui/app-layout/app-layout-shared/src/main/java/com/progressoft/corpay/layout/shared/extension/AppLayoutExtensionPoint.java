package com.progressoft.corpay.layout.shared.extension;

import org.dominokit.domino.api.shared.extension.ExtensionPoint;

public interface AppLayoutExtensionPoint extends ExtensionPoint<AppLayoutContext>{
}
