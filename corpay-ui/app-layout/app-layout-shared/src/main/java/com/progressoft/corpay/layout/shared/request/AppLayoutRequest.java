package com.progressoft.corpay.layout.shared.request;

import org.dominokit.domino.api.shared.request.RequestBean;

public class AppLayoutRequest implements RequestBean {

    private String message;

    public AppLayoutRequest() {
    }

    public AppLayoutRequest(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
