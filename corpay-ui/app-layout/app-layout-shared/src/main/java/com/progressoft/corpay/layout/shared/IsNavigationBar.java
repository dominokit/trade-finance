package com.progressoft.corpay.layout.shared;

import org.dominokit.domino.api.shared.extension.Content;

public interface IsNavigationBar {

    boolean isCollapsed();

    IsNavigationBar setCollapsed(boolean collapsed);

    Content getMenu();

    Content getNavBarExpand();

    Content getNavigationBar();

    Content getTopBar();

    Content getPrimaryNavigationBar();

    Content getPrimaryTopBar();

    Content getTitle();
}
