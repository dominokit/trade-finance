package com.progressoft.corpay.layout.shared.response;

import org.dominokit.domino.api.shared.request.ResponseBean;

public class AppLayoutResponse implements ResponseBean {

    private String serverMessage;

    public AppLayoutResponse() {
    }

    public AppLayoutResponse(String serverMessage) {
        this.serverMessage = serverMessage;
    }

    public String getServerMessage() {
        return serverMessage;
    }

    public void setServerMessage(String serverMessage) {
        this.serverMessage = serverMessage;
    }
}
