package com.progressoft.corpay.layout.shared.extension;

import com.progressoft.corpay.layout.shared.IsLayout;
import org.dominokit.domino.api.shared.extension.Context;

@FunctionalInterface
public interface AppLayoutContext extends Context {
    IsLayout getLayout();
}
