package com.progressoft.corpay.layout.server.handlers;

import com.progressoft.corpay.layout.shared.request.AppLayoutRequest;
import com.progressoft.corpay.layout.shared.response.AppLayoutResponse;
import javax.annotation.Generated;
import org.dominokit.domino.api.server.endpoint.AbstractEndpoint;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.server.EndpointsProcessor")
public class AppLayoutHandlerEndpointHandler extends AbstractEndpoint<AppLayoutRequest, AppLayoutResponse> {
  @Override
  protected AppLayoutRequest makeNewRequest() {
    return new AppLayoutRequest();
  }

  @Override
  protected Class<AppLayoutRequest> getRequestClass() {
    return AppLayoutRequest.class;
  }
}
