package com.progressoft.corpay.layout.server;

import com.google.auto.service.AutoService;
import com.progressoft.corpay.layout.server.handlers.AppLayoutHandler;
import com.progressoft.corpay.layout.server.handlers.AppLayoutHandlerEndpointHandler;
import java.util.function.Supplier;
import javax.annotation.Generated;
import org.dominokit.domino.api.server.config.ServerModuleConfiguration;
import org.dominokit.domino.api.server.endpoint.EndpointsRegistry;
import org.dominokit.domino.api.server.handler.HandlerRegistry;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.server.ServerModuleAnnotationProcessor")
@AutoService(ServerModuleConfiguration.class)
public class AppLayoutServerModule implements ServerModuleConfiguration {
  @Override
  public void registerHandlers(HandlerRegistry registry) {
    registry.registerHandler("AppLayoutRequest",new AppLayoutHandler());
  }

  @Override
  public void registerEndpoints(EndpointsRegistry registry) {
    registry.registerEndpoint("AppLayoutRequest", new Supplier<AppLayoutHandlerEndpointHandler>() {
      @Override
      public AppLayoutHandlerEndpointHandler get() {
        return new AppLayoutHandlerEndpointHandler();
      }
    });
  }
}
