package com.progressoft.corpay.layout.server.handlers;

import org.dominokit.domino.api.server.handler.Handler;
import org.dominokit.domino.api.server.handler.RequestHandler;
import org.dominokit.domino.api.server.context.ExecutionContext;
import com.progressoft.corpay.layout.shared.response.AppLayoutResponse;
import com.progressoft.corpay.layout.shared.request.AppLayoutRequest;

import java.util.logging.Logger;

@Handler("AppLayoutRequest")
public class AppLayoutHandler implements RequestHandler<AppLayoutRequest, AppLayoutResponse> {
    private static final Logger LOGGER= Logger.getLogger(AppLayoutHandler.class.getName());
    @Override
    public void handleRequest(ExecutionContext<AppLayoutRequest, AppLayoutResponse> executionContext) {
        LOGGER.info("message recieved from client : "+executionContext.getRequestBean().getMessage());
        executionContext.end(new AppLayoutResponse("Server message"));
    }
}
