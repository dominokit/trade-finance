package com.progressoft.corpay.layout.client.ui.views.layout;

import elemental2.dom.DomGlobal;
import org.jboss.gwt.elemento.template.TemplateUtil;

import javax.annotation.Generated;

/*
 * WARNING! This class is generated. Do not modify.
 */
@Generated("org.jboss.gwt.elemento.processor.TemplatedProcessor")
public final class Templated_Section extends Section {

    private final elemental2.dom.HTMLElement templated_section_root_element;

 public Templated_Section() {

        this.templated_section_root_element = (elemental2.dom.HTMLElement)DomGlobal.document.createElement("section");
        this.templated_section_root_element.innerHTML = "<!-- Left Sidebar --> <aside data-element=\"leftSide\" id=\"leftsidebar\" class=\"sidebar\">  <!-- #Footer --> </aside> <!-- #END# Left Sidebar --> <!-- Right Sidebar --> <aside data-element=\"rightSide\" id=\"rightsidebar\" class=\"right-sidebar overlay-open\" style=\"right: 0px !important; height: calc(vh -70px); overflow-y: scroll;\"> </aside> <!-- #END# Right Sidebar -->";

        if (this.leftSide == null) {
            this.leftSide = TemplateUtil.resolveElement(templated_section_root_element, "leftSide");
        } else {
            TemplateUtil.replaceElement(templated_section_root_element, "leftSide", leftSide);
        }
        if (this.rightSide == null) {
            this.rightSide = TemplateUtil.resolveElement(templated_section_root_element, "rightSide");
        } else {
            TemplateUtil.replaceElement(templated_section_root_element, "rightSide", rightSide);
        }
    }

    @Override
    public elemental2.dom.HTMLElement asElement() {
        return templated_section_root_element;
    }
}
