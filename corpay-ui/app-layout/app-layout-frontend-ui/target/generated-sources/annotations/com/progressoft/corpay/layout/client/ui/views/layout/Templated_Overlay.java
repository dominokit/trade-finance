package com.progressoft.corpay.layout.client.ui.views.layout;

import elemental2.dom.DomGlobal;

import javax.annotation.Generated;

/*
 * WARNING! This class is generated. Do not modify.
 */
@Generated("org.jboss.gwt.elemento.processor.TemplatedProcessor")
public final class Templated_Overlay extends Overlay {

    private final elemental2.dom.HTMLDivElement templated_overlay_root_element;

 public Templated_Overlay() {

        this.templated_overlay_root_element = (elemental2.dom.HTMLDivElement)DomGlobal.document.createElement("div");
        this.templated_overlay_root_element.setAttribute("class", "overlay");
        this.templated_overlay_root_element.setAttribute("style", "display: none;");

    }

    @Override
    public elemental2.dom.HTMLDivElement asElement() {
        return templated_overlay_root_element;
    }
}
