package com.progressoft.corpay.layout.client;

import com.progressoft.corpay.layout.client.presenters.AppLayoutPresenter;
import com.progressoft.corpay.layout.client.ui.views.AppLayoutViewImpl;
import javax.annotation.Generated;
import org.dominokit.domino.api.client.ModuleConfiguration;
import org.dominokit.domino.api.client.mvp.ViewRegistry;
import org.dominokit.domino.api.client.mvp.view.LazyViewLoader;
import org.dominokit.domino.api.client.mvp.view.View;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.module.client.ClientModuleAnnotationProcessor")
public class AppLayoutUIModuleConfiguration implements ModuleConfiguration {
  @Override
  public void registerViews(ViewRegistry registry) {
    registry.registerView(new LazyViewLoader(AppLayoutPresenter.class.getCanonicalName()) {
      @Override
      protected View make() {
        return new AppLayoutViewImpl();
      }
    });
  }
}
