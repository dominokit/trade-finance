package com.progressoft.corpay.layout.client.ui.views.layout;

import elemental2.dom.DomGlobal;
import org.jboss.gwt.elemento.template.TemplateUtil;

import javax.annotation.Generated;

/*
 * WARNING! This class is generated. Do not modify.
 */
@Generated("org.jboss.gwt.elemento.processor.TemplatedProcessor")
public final class Templated_ContentPanel extends ContentPanel {

    private final elemental2.dom.HTMLElement templated_contentpanel_root_element;

 public Templated_ContentPanel() {

        this.templated_contentpanel_root_element = (elemental2.dom.HTMLElement)DomGlobal.document.createElement("section");
        this.templated_contentpanel_root_element.setAttribute("class", "content");
        this.templated_contentpanel_root_element.innerHTML = "<div data-element=\"contentContainer\" class=\"content-panel\"> </div>";

        if (this.contentContainer == null) {
            this.contentContainer = TemplateUtil.<elemental2.dom.HTMLDivElement>resolveElementAs(templated_contentpanel_root_element, "contentContainer");
        } else {
            TemplateUtil.replaceElement(templated_contentpanel_root_element, "contentContainer", contentContainer);
        }
    }

    @Override
    public elemental2.dom.HTMLElement asElement() {
        return templated_contentpanel_root_element;
    }
}
