package com.progressoft.corpay.layout.client.ui.views.layout;

import elemental2.dom.DomGlobal;
import org.jboss.gwt.elemento.template.TemplateUtil;

import javax.annotation.Generated;

/*
 * WARNING! This class is generated. Do not modify.
 */
@Generated("org.jboss.gwt.elemento.processor.TemplatedProcessor")
public final class Templated_NavigationBar extends NavigationBar {

    private final elemental2.dom.HTMLElement templated_navigationbar_root_element;

 public Templated_NavigationBar() {

        this.templated_navigationbar_root_element = (elemental2.dom.HTMLElement)DomGlobal.document.createElement("nav");
        this.templated_navigationbar_root_element.setAttribute("class", "navbar bars");
        this.templated_navigationbar_root_element.innerHTML = "<div class=\"container-fluid\">  <div class=\"navbar-header\">   <a data-element=\"navBarExpand\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar-collapse\" aria-expanded=\"false\"></a>   <a data-element=\"menu\" class=\"bars\"></a>   <a data-element=\"title\" class=\"navbar-brand\"></a>  </div>  <div data-element=\"navigationBar\" class=\"collapse navbar-collapse\" id=\"navbar-collapse\">   <ul data-element=\"topBar\" class=\"nav navbar-nav navbar-right\">   </ul>  </div> </div> <div class=\"primary-navbar\">  <div data-element=\"primaryNavigationBar\" id=\"primary-navbar-collapse\">   <ul data-element=\"primaryTopBar\" class=\"nav navbar-nav navbar-right\">   </ul>  </div> </div>";

        if (this.menu == null) {
            this.menu = TemplateUtil.<elemental2.dom.HTMLAnchorElement>resolveElementAs(templated_navigationbar_root_element, "menu");
        } else {
            TemplateUtil.replaceElement(templated_navigationbar_root_element, "menu", menu);
        }
        if (this.navBarExpand == null) {
            this.navBarExpand = TemplateUtil.<elemental2.dom.HTMLAnchorElement>resolveElementAs(templated_navigationbar_root_element, "navBarExpand");
        } else {
            TemplateUtil.replaceElement(templated_navigationbar_root_element, "navBarExpand", navBarExpand);
        }
        if (this.navigationBar == null) {
            this.navigationBar = TemplateUtil.<elemental2.dom.HTMLDivElement>resolveElementAs(templated_navigationbar_root_element, "navigationBar");
        } else {
            TemplateUtil.replaceElement(templated_navigationbar_root_element, "navigationBar", navigationBar);
        }
        if (this.topBar == null) {
            this.topBar = TemplateUtil.<elemental2.dom.HTMLUListElement>resolveElementAs(templated_navigationbar_root_element, "topBar");
        } else {
            TemplateUtil.replaceElement(templated_navigationbar_root_element, "topBar", topBar);
        }
        if (this.primaryNavigationBar == null) {
            this.primaryNavigationBar = TemplateUtil.<elemental2.dom.HTMLDivElement>resolveElementAs(templated_navigationbar_root_element, "primaryNavigationBar");
        } else {
            TemplateUtil.replaceElement(templated_navigationbar_root_element, "primaryNavigationBar", primaryNavigationBar);
        }
        if (this.primaryTopBar == null) {
            this.primaryTopBar = TemplateUtil.<elemental2.dom.HTMLUListElement>resolveElementAs(templated_navigationbar_root_element, "primaryTopBar");
        } else {
            TemplateUtil.replaceElement(templated_navigationbar_root_element, "primaryTopBar", primaryTopBar);
        }
        if (this.title == null) {
            this.title = TemplateUtil.<elemental2.dom.HTMLAnchorElement>resolveElementAs(templated_navigationbar_root_element, "title");
        } else {
            TemplateUtil.replaceElement(templated_navigationbar_root_element, "title", title);
        }
    }

    @Override
    public elemental2.dom.HTMLElement asElement() {
        return templated_navigationbar_root_element;
    }
}
