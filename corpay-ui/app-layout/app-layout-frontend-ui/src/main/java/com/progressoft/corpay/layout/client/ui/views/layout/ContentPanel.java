package com.progressoft.corpay.layout.client.ui.views.layout;

import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLElement;
import org.jboss.gwt.elemento.core.IsElement;
import org.jboss.gwt.elemento.template.DataElement;
import org.jboss.gwt.elemento.template.Templated;

@Templated
public abstract class ContentPanel implements IsElement<HTMLElement>{

    @DataElement
    HTMLDivElement contentContainer;

    public static ContentPanel create(){
        return new Templated_ContentPanel();
    }
}
