package com.progressoft.corpay.layout.client.ui.views.layout;

import com.progressoft.corpay.layout.shared.IsNavigationBar;
import elemental2.dom.HTMLAnchorElement;
import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLElement;
import elemental2.dom.HTMLUListElement;
import org.dominokit.domino.api.shared.extension.Content;
import org.jboss.gwt.elemento.core.IsElement;
import org.jboss.gwt.elemento.template.DataElement;
import org.jboss.gwt.elemento.template.Templated;

@Templated
public abstract class NavigationBar implements IsElement<HTMLElement>, IsNavigationBar {

    @DataElement
    HTMLAnchorElement menu;

    @DataElement
    HTMLAnchorElement navBarExpand;

    @DataElement
    HTMLDivElement navigationBar;

    @DataElement
    HTMLUListElement topBar;

    @DataElement
    HTMLDivElement primaryNavigationBar;

    @DataElement
    HTMLUListElement primaryTopBar;

    @DataElement
    HTMLAnchorElement title;

    private boolean collapsed = true;

    public static NavigationBar create() {
        return new Templated_NavigationBar();
    }

    public boolean isCollapsed() {
        return collapsed;
    }

    public NavigationBar setCollapsed(boolean collapsed) {
        this.collapsed = collapsed;
        return this;
    }

    public Content getMenu() {
        return () -> menu;
    }

    public Content getNavBarExpand() {
        return () -> navBarExpand;
    }

    public Content getNavigationBar() {
        return () -> navigationBar;
    }

    public Content getTopBar() {
        return () -> topBar;
    }

    public Content getPrimaryNavigationBar() {
        return () -> primaryNavigationBar;
    }

    public Content getPrimaryTopBar() {
        return () -> primaryTopBar;
    }

    public Content getTitle() {
        return () -> title;
    }

}
