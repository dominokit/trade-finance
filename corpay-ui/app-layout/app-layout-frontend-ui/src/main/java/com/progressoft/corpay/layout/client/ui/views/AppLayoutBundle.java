package com.progressoft.corpay.layout.client.ui.views;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;

public interface AppLayoutBundle extends ClientBundle{

    interface Style extends CssResource {
        String AppLayout();
    }

    @Source("css/AppLayout.gss")
    Style style();
}