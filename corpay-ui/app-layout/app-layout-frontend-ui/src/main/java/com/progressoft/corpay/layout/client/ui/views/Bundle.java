package com.progressoft.corpay.layout.client.ui.views;

import com.google.gwt.core.client.GWT;

public class Bundle {

    public static final AppLayoutBundle INSTANCE= GWT.create(AppLayoutBundle.class);

    private Bundle() {
    }
}
