package com.progressoft.corpay.layout.client.ui.views;

import com.progressoft.corpay.layout.client.presenters.AppLayoutPresenter;
import com.progressoft.corpay.layout.client.ui.views.layout.Layout;
import com.progressoft.corpay.layout.client.views.AppLayoutView;
import com.progressoft.corpay.layout.shared.IsLayout;
import elemental2.dom.DomGlobal;
import org.dominokit.domino.api.client.annotations.UiView;
import org.dominokit.domino.ui.themes.Theme;

@UiView(presentable = AppLayoutPresenter.class)
public class AppLayoutViewImpl implements AppLayoutView {

    private Layout layout = Layout.create();

    public AppLayoutViewImpl() {
        DomGlobal.document.body.style.background = "#e9e9e9";
    }

    @Override
    public IsLayout getLayout() {
        return layout;
    }

    @Override
    public void show() {
        layout.show(Theme.INDIGO.color().getName());
    }
}