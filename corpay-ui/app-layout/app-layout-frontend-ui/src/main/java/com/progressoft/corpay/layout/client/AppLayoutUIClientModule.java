package com.progressoft.corpay.layout.client;

import com.google.gwt.core.client.EntryPoint;
import org.dominokit.domino.api.client.ModuleConfigurator;
import org.dominokit.domino.api.client.annotations.ClientModule;
import com.progressoft.corpay.layout.client.ui.views.Bundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.corpay.layout.client.ui.views.AppLayoutBundle;

@ClientModule(name="AppLayoutUI")
public class AppLayoutUIClientModule implements EntryPoint {

	private static final Logger LOGGER = LoggerFactory.getLogger(AppLayoutUIClientModule.class);

	public void onModuleLoad() {
		LOGGER.info("Initializing AppLayout frontend UI module ...");
		Bundle.INSTANCE.style().ensureInjected();
		new ModuleConfigurator().configureModule(new AppLayoutUIModuleConfiguration());
	}
}
