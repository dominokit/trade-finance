package com.progressoft.corpay.layout.client.ui.views.layout;

import com.progressoft.corpay.layout.shared.IsLayout;
import com.progressoft.corpay.layout.shared.IsNavigationBar;
import elemental2.dom.CSSProperties;
import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLLIElement;
import elemental2.dom.Text;
import jsinterop.base.Js;
import org.dominokit.domino.api.shared.extension.Content;
import org.dominokit.domino.ui.icons.Icon;
import org.dominokit.domino.ui.style.ColorScheme;
import org.dominokit.domino.ui.style.Style;
import org.dominokit.domino.ui.themes.Theme;
import org.dominokit.domino.ui.utils.ElementUtil;

import static elemental2.dom.DomGlobal.document;
import static org.jboss.gwt.elemento.core.Elements.a;
import static org.jboss.gwt.elemento.core.Elements.div;
import static org.jboss.gwt.elemento.core.Elements.li;

public class Layout implements IsLayout {

    private static final String SLIDE_OUT = "-300px";
    private static final String SLIDE_IN = "0px";
    private static final String NONE = "none";
    private static final String BLOCK = "block";
    private static final String COLLAPSE = "collapse";
    private static final String CLICK = "click";

    private final NavigationBar productNavigation = NavigationBar.create();
    private final Section section = Section.create();
    private final Overlay overlay = Overlay.create();
    private final ContentPanel content = ContentPanel.create();
    private final HTMLDivElement pageHeaderContainer = div().css("app-page-header").asElement();

    private Text appTitle = new Text("");

    private boolean leftPanelVisible = false;
    private boolean rightPanelVisible = false;
    private boolean navigationBarExpanded = false;
    private boolean overlayVisible = false;
    private boolean fixedLeftPanel;

    public Layout() {
    }

    public Layout(String title) {
        setTitle(title);
    }

    public static Layout create() {
        return new Layout();
    }

    public static Layout create(String title) {
        return new Layout(title);
    }

    @Override
    public Layout show() {
        return show(ColorScheme.INDIGO.toString());
    }

    @Override
    public Layout show(String theme) {
        appendElements();
        initElementsPosition();
        addExpandListeners();
        if (!document.body.classList.contains("ls-hidden"))
            document.body.classList.add("ls-closed");
        new Theme(ColorScheme.valueOf(theme)).apply();
        return this;
    }

    private void appendElements() {
        document.body.appendChild(overlay.asElement());
        document.body.appendChild(productNavigation.asElement());
        document.body.appendChild(pageHeaderContainer);
        document.body.appendChild(section.asElement());
        document.body.appendChild(content.asElement());
        Style.of(content).setMarginTop("120px");
        productNavigation.title.appendChild(appTitle);
        Style.of(pageHeaderContainer).setBackgroundColor("white");
        hidePageHeader();
    }

    private void initElementsPosition() {
        section.leftSide.style.marginLeft = CSSProperties.MarginLeftUnionType.of(0);
        section.rightSide.style.marginRight = CSSProperties.MarginRightUnionType.of(0);
        section.leftSide.style.left = SLIDE_OUT;
        section.rightSide.style.right = SLIDE_OUT;

        section.leftSide.style.height = CSSProperties.HeightUnionType.of("calc(100vh - 100px)");
        section.leftSide.style.top = "100px";
        section.leftSide.style.width = CSSProperties.WidthUnionType.of("250px");
    }

    private void addExpandListeners() {
        productNavigation.menu.addEventListener(CLICK, e -> toggleLeftPanel());
        productNavigation.navBarExpand.addEventListener(CLICK, e -> toggleNavigationBar());
        overlay.asElement().addEventListener(CLICK, e -> hidePanels());
    }

    public Layout removeLeftPanel() {
        return updateLeftPanel("none", "ls-closed", "ls-hidden");
    }

    public Layout addLeftPanel() {
        return updateLeftPanel("block", "ls-hidden", "ls-closed");
    }

    public Layout updateLeftPanel(String none, String hiddenStyle, String visibleStyle) {
        productNavigation.menu.style.display = none;
        section.leftSide.style.display = none;
        document.body.classList.remove(hiddenStyle);
        document.body.classList.add(visibleStyle);

        return this;
    }

    private void hidePanels() {
        hideRightPanel();
        hideLeftPanel();
        collapseNavBar();
        hideOverlay();
    }

    private void toggleNavigationBar() {
        if (navigationBarExpanded)
            collapseNavBar();
        else
            expandNavBar();
    }

    private void expandNavBar() {
        if (leftPanelVisible)
            hideLeftPanel();
        if (rightPanelVisible)
            hideRightPanel();
        productNavigation.navigationBar.classList.remove(COLLAPSE);
        navigationBarExpanded = true;
    }

    private void collapseNavBar() {
        productNavigation.navigationBar.classList.add(COLLAPSE);
        navigationBarExpanded = false;
    }

    @Override
    public void toggleRightPanel() {
        if (rightPanelVisible)
            hideRightPanel();
        else
            showRightPanel();
    }

    @Override
    public IsLayout showRightPanel() {
        if (leftPanelVisible)
            hideLeftPanel();
        if (navigationBarExpanded)
            collapseNavBar();
        section.rightSide.style.right = SLIDE_IN;
        rightPanelVisible = true;
        showOverlay();

        return this;
    }

    @Override
    public IsLayout hideRightPanel() {
        section.rightSide.style.right = SLIDE_OUT;
        rightPanelVisible = false;
        hideOverlay();

        return this;
    }

    private void hideOverlay() {
        if (overlayVisible) {
            overlay.asElement().style.display = NONE;
            overlayVisible = false;
        }
    }

    private void showOverlay() {
        if (!overlayVisible) {
            overlay.asElement().style.display = BLOCK;
            overlayVisible = true;
        }
    }

    @Override
    public void toggleLeftPanel() {
        if (leftPanelVisible)
            hideLeftPanel();
        else
            showLeftPanel();
    }

    @Override
    public IsLayout showLeftPanel() {
        if (rightPanelVisible)
            hideRightPanel();
        if (navigationBarExpanded)
            collapseNavBar();
        section.leftSide.style.left = SLIDE_IN;
        leftPanelVisible = true;
        showOverlay();

        return this;
    }

    @Override
    public IsLayout hideLeftPanel() {
        if (!fixedLeftPanel) {
            section.leftSide.style.left = SLIDE_OUT;
            leftPanelVisible = false;
            hideOverlay();
        }

        return this;
    }

    @Override
    public Content getRightPanel() {
        return () -> section.rightSide;
    }

    @Override
    public Content getLeftPanel() {
        return () -> section.leftSide;
    }

    @Override
    public Content getContentPanel() {
        return () -> content.contentContainer;
    }

    @Override
    public Content getTopBar() {
        return () -> productNavigation.topBar;
    }

    @Override
    public IsNavigationBar getProductNavigation() {
        return this.productNavigation;
    }

    @Override
    public Content getContentSection() {
        return () -> this.content;
    }

    @Override
    public Layout setTitle(String title) {
        if (productNavigation.title.hasChildNodes())
            productNavigation.title.removeChild(appTitle);
        this.appTitle = new Text(title);
        productNavigation.title.appendChild(appTitle);
        Style.of(productNavigation.title)
                .setHeight("40px")
                .setProperty("line-height", "10px");

        return this;
    }

    @Override
    public Content addActionItem(String iconName) {
        HTMLLIElement li = li().css("pull-right").add(
                a().css("js-right-sidebar")
                        .add(Icon.create(iconName).asElement())).asElement();
        productNavigation.topBar.appendChild(li);
        return () -> li;
    }

    @Override
    public void showPageHeader() {
        Style.of(pageHeaderContainer).setDisplay("block");
    }

    @Override
    public void hidePageHeader() {
        Style.of(pageHeaderContainer).setDisplay("none");
    }

    @Override
    public void setPageHeaderContent(Content content) {
        ElementUtil.clear(pageHeaderContainer);
        pageHeaderContainer.appendChild(Js.cast(content.get()));
    }

    @Override
    public Content getPageHeaderContainer() {
        return () -> pageHeaderContainer;
    }

    @Override
    public IsLayout fixLeftPanelPosition() {
        showLeftPanel();
        hideOverlay();
        if (document.body.classList.contains("ls-closed"))
            document.body.classList.remove("ls-closed");
        this.fixedLeftPanel = true;
        return this;
    }

    @Override
    public IsLayout unfixLeftPanelPosition() {
        if (!document.body.classList.contains("ls-closed"))
            document.body.classList.add("ls-closed");
        this.fixedLeftPanel = false;
        return this;
    }
}
