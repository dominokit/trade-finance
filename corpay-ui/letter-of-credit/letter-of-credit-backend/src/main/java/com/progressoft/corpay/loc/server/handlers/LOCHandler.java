package com.progressoft.corpay.loc.server.handlers;

import org.dominokit.domino.api.server.handler.Handler;
import org.dominokit.domino.api.server.handler.RequestHandler;
import org.dominokit.domino.api.server.context.ExecutionContext;
import com.progressoft.corpay.loc.shared.response.LOCResponse;
import com.progressoft.corpay.loc.shared.request.LOCRequest;

import java.util.logging.Logger;

@Handler("LOCRequest")
public class LOCHandler implements RequestHandler<LOCRequest, LOCResponse> {
    private static final Logger LOGGER= Logger.getLogger(LOCHandler.class.getName());
    @Override
    public void handleRequest(ExecutionContext<LOCRequest, LOCResponse> executionContext) {
        LOGGER.info("message recieved from client : "+executionContext.getRequestBean().getMessage());
        executionContext.end(new LOCResponse("Server message"));
    }
}
