package com.progressoft.corpay.loc.server;

import com.google.auto.service.AutoService;
import com.progressoft.corpay.loc.server.handlers.LOCHandler;
import com.progressoft.corpay.loc.server.handlers.LOCHandlerEndpointHandler;
import java.util.function.Supplier;
import javax.annotation.Generated;
import org.dominokit.domino.api.server.config.ServerModuleConfiguration;
import org.dominokit.domino.api.server.endpoint.EndpointsRegistry;
import org.dominokit.domino.api.server.handler.HandlerRegistry;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.server.ServerModuleAnnotationProcessor")
@AutoService(ServerModuleConfiguration.class)
public class LOCServerModule implements ServerModuleConfiguration {
  @Override
  public void registerHandlers(HandlerRegistry registry) {
    registry.registerHandler("LOCRequest",new LOCHandler());
  }

  @Override
  public void registerEndpoints(EndpointsRegistry registry) {
    registry.registerEndpoint("LOCRequest", new Supplier<LOCHandlerEndpointHandler>() {
      @Override
      public LOCHandlerEndpointHandler get() {
        return new LOCHandlerEndpointHandler();
      }
    });
  }
}
