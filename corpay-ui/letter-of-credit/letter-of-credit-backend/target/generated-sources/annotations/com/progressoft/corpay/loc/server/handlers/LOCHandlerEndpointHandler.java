package com.progressoft.corpay.loc.server.handlers;

import com.progressoft.corpay.loc.shared.request.LOCRequest;
import com.progressoft.corpay.loc.shared.response.LOCResponse;
import javax.annotation.Generated;
import org.dominokit.domino.api.server.endpoint.AbstractEndpoint;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.server.EndpointsProcessor")
public class LOCHandlerEndpointHandler extends AbstractEndpoint<LOCRequest, LOCResponse> {
  @Override
  protected LOCRequest makeNewRequest() {
    return new LOCRequest();
  }

  @Override
  protected Class<LOCRequest> getRequestClass() {
    return LOCRequest.class;
  }
}
