package com.progressoft.corpay.loc.shared.request;

import org.dominokit.domino.api.shared.request.RequestBean;

public class LOCRequest implements RequestBean {

    private String message;

    public LOCRequest() {
    }

    public LOCRequest(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
