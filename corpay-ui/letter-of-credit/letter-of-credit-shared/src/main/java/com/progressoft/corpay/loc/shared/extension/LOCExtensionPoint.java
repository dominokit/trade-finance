package com.progressoft.corpay.loc.shared.extension;

import org.dominokit.domino.api.shared.extension.ExtensionPoint;

public interface LOCExtensionPoint extends ExtensionPoint<LOCContext> {
}
