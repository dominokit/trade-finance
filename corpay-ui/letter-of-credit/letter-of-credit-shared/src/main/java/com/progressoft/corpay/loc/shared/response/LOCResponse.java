package com.progressoft.corpay.loc.shared.response;

import org.dominokit.domino.api.shared.request.ResponseBean;

public class LOCResponse implements ResponseBean {

    private String serverMessage;

    public LOCResponse() {
    }

    public LOCResponse(String serverMessage) {
        this.serverMessage = serverMessage;
    }

    public String getServerMessage() {
        return serverMessage;
    }

    public void setServerMessage(String serverMessage) {
        this.serverMessage = serverMessage;
    }
}
