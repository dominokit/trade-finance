package com.progressoft.corpay.loc.shared.extension;

import com.progressoft.corpay.tradefinance.shared.extension.TradeFinanceContext;
import org.dominokit.domino.api.shared.extension.Context;

public interface LOCContext extends Context {
    String PATH = TradeFinanceContext.PATH + "/" + "Letter of credit";

    TradeFinanceContext getTradeFinanceContext();
}
