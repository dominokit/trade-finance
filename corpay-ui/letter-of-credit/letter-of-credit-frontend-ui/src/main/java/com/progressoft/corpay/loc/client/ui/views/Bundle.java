package com.progressoft.corpay.loc.client.ui.views;

import com.google.gwt.core.client.GWT;

public class Bundle {

    public static final LOCBundle INSTANCE= GWT.create(LOCBundle.class);

    private Bundle() {
    }
}
