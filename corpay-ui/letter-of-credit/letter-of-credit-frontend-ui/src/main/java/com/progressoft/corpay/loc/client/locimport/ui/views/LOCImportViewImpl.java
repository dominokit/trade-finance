package com.progressoft.corpay.loc.client.locimport.ui.views;

import com.progressoft.corpay.loc.client.locimport.presenters.LOCImportPresenter;
import com.progressoft.corpay.loc.client.locimport.views.LOCImportView;
import org.dominokit.domino.api.client.annotations.UiView;
import org.dominokit.domino.api.shared.extension.Content;
import org.dominokit.domino.ui.cards.Card;

@UiView(presentable = LOCImportPresenter.class)
public class LOCImportViewImpl implements LOCImportView {

    @Override
    public Content getContent() {
        return () -> Card.create("Letter of credit import").asElement();
    }
}
