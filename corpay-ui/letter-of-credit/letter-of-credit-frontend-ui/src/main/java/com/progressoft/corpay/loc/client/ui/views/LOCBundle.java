package com.progressoft.corpay.loc.client.ui.views;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;

public interface LOCBundle extends ClientBundle{

    interface Style extends CssResource {
        String LOC();
    }

    @Source("css/LOC.gss")
    Style style();
}