package com.progressoft.corpay.loc.client;

import com.google.gwt.core.client.EntryPoint;
import org.dominokit.domino.api.client.ModuleConfigurator;
import org.dominokit.domino.api.client.annotations.ClientModule;
import com.progressoft.corpay.loc.client.ui.views.Bundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.corpay.loc.client.ui.views.LOCBundle;

@ClientModule(name="LOCUI")
public class LOCUIClientModule implements EntryPoint {

	private static final Logger LOGGER = LoggerFactory.getLogger(LOCUIClientModule.class);

	public void onModuleLoad() {
		LOGGER.info("Initializing LOC frontend UI module ...");
		Bundle.INSTANCE.style().ensureInjected();
		new ModuleConfigurator().configureModule(new LOCUIModuleConfiguration());
	}
}
