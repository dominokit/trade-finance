package com.progressoft.corpay.loc.client.locexport.ui.views;

import com.progressoft.corpay.loc.client.locexport.presenters.LOCExportPresenter;
import com.progressoft.corpay.loc.client.locexport.views.LOCExportView;
import org.dominokit.domino.api.client.annotations.UiView;
import org.dominokit.domino.api.shared.extension.Content;
import org.dominokit.domino.ui.cards.Card;

@UiView(presentable = LOCExportPresenter.class)
public class LOCExportViewImpl implements LOCExportView {

    @Override
    public Content getContent() {
        return () -> Card.create("Export").asElement();
    }
}
