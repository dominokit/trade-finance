package com.progressoft.corpay.loc.client.ui.views;

import com.progressoft.corpay.loc.client.presenters.LOCPresenter;
import com.progressoft.corpay.loc.client.views.LOCView;
import org.dominokit.domino.api.client.annotations.UiView;
import org.dominokit.domino.api.shared.extension.Content;
import org.dominokit.domino.ui.cards.Card;

@UiView(presentable = LOCPresenter.class)
public class LOCViewImpl implements LOCView {

    @Override
    public Content getContent() {
        return () -> Card.create("Letter of credit").asElement();
    }
}