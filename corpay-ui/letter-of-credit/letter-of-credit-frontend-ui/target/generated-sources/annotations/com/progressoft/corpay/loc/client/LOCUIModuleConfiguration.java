package com.progressoft.corpay.loc.client;

import com.progressoft.corpay.loc.client.locexport.presenters.LOCExportPresenter;
import com.progressoft.corpay.loc.client.locexport.ui.views.LOCExportViewImpl;
import com.progressoft.corpay.loc.client.locimport.presenters.LOCImportPresenter;
import com.progressoft.corpay.loc.client.locimport.ui.views.LOCImportViewImpl;
import com.progressoft.corpay.loc.client.presenters.LOCPresenter;
import com.progressoft.corpay.loc.client.ui.views.LOCViewImpl;
import javax.annotation.Generated;
import org.dominokit.domino.api.client.ModuleConfiguration;
import org.dominokit.domino.api.client.mvp.ViewRegistry;
import org.dominokit.domino.api.client.mvp.view.LazyViewLoader;
import org.dominokit.domino.api.client.mvp.view.View;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.module.client.ClientModuleAnnotationProcessor")
public class LOCUIModuleConfiguration implements ModuleConfiguration {
  @Override
  public void registerViews(ViewRegistry registry) {
    registry.registerView(new LazyViewLoader(LOCExportPresenter.class.getCanonicalName()) {
      @Override
      protected View make() {
        return new LOCExportViewImpl();
      }
    });
    registry.registerView(new LazyViewLoader(LOCImportPresenter.class.getCanonicalName()) {
      @Override
      protected View make() {
        return new LOCImportViewImpl();
      }
    });
    registry.registerView(new LazyViewLoader(LOCPresenter.class.getCanonicalName()) {
      @Override
      protected View make() {
        return new LOCViewImpl();
      }
    });
  }
}
