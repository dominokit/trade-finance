package com.progressoft.corpay.loc.client.requests;

import org.dominokit.domino.api.client.annotations.Path;
import org.dominokit.domino.api.client.annotations.RequestFactory;
import org.dominokit.domino.api.client.request.Response;
import com.progressoft.corpay.loc.shared.response.LOCResponse;
import com.progressoft.corpay.loc.shared.request.LOCRequest;

@RequestFactory
public interface LOCRequests {
    @Path("LOCRequest")
    Response<LOCResponse> request(LOCRequest request);
}
