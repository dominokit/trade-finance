package com.progressoft.corpay.loc.client.locexport.presenters;

import com.progressoft.corpay.loc.client.locexport.views.LOCExportView;
import org.dominokit.domino.api.client.annotations.Presenter;
import org.dominokit.domino.api.client.mvp.presenter.BaseClientPresenter;

@Presenter
public class LOCExportPresenter extends BaseClientPresenter<LOCExportView> {

}
