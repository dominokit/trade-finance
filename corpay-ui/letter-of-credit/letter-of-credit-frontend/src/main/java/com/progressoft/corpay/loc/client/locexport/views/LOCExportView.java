package com.progressoft.corpay.loc.client.locexport.views;

import com.progressoft.corpay.componentcase.shared.extension.HasContent;
import org.dominokit.domino.api.client.mvp.view.View;

public interface LOCExportView extends View, HasContent {
}
