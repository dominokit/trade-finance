package com.progressoft.corpay.loc.client.views;

import com.progressoft.corpay.componentcase.shared.extension.HasContent;
import org.dominokit.domino.api.client.mvp.view.View;

public interface LOCView extends View, HasContent {
}