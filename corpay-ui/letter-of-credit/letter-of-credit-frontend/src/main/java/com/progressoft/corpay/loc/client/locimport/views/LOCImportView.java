package com.progressoft.corpay.loc.client.locimport.views;

import com.progressoft.corpay.componentcase.shared.extension.HasContent;
import org.dominokit.domino.api.client.mvp.view.View;

public interface LOCImportView extends View, HasContent {
}
