package com.progressoft.corpay.loc.client.locimport.presenters;

import com.progressoft.corpay.componentcase.shared.extension.AppModuleContext;
import com.progressoft.corpay.loc.client.locimport.views.LOCImportView;
import com.progressoft.corpay.loc.shared.extension.LOCContext;
import com.progressoft.corpay.loc.shared.extension.LOCExtensionPoint;
import org.dominokit.domino.api.client.annotations.InjectContext;
import org.dominokit.domino.api.client.annotations.Presenter;
import org.dominokit.domino.api.client.mvp.presenter.BaseClientPresenter;
import org.dominokit.domino.api.shared.extension.Content;

@Presenter
public class LOCImportPresenter extends BaseClientPresenter<LOCImportView> {

    @InjectContext(extensionPoint = LOCExtensionPoint.class)
    public void contributeToLOCModule(LOCContext context) {
        context.getTradeFinanceContext().getAppModuleContext().addModule(new AppModuleContext.AppModule() {
            @Override
            public String getModuleName() {
                return "Import";
            }

            @Override
            public String getHistoryToken() {
                return "import";
            }

            @Override
            public String getPath() {
                return LOCContext.PATH + "/" + getModuleName();
            }

            @Override
            public boolean hasContent() {
                return true;
            }

            @Override
            public Content getContent() {
                return view.getContent();
            }
        });
    }
}
