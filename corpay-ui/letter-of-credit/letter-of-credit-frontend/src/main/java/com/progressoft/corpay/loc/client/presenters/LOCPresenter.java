package com.progressoft.corpay.loc.client.presenters;

import com.progressoft.corpay.componentcase.shared.extension.AppModuleContext;
import com.progressoft.corpay.loc.client.views.LOCView;
import com.progressoft.corpay.loc.shared.extension.LOCContext;
import com.progressoft.corpay.loc.shared.extension.LOCExtensionPoint;
import com.progressoft.corpay.tradefinance.shared.extension.TradeFinanceContext;
import com.progressoft.corpay.tradefinance.shared.extension.TradeFinanceExtensionPoint;
import org.dominokit.domino.api.client.annotations.InjectContext;
import org.dominokit.domino.api.client.annotations.Presenter;
import org.dominokit.domino.api.client.mvp.presenter.BaseClientPresenter;
import org.dominokit.domino.api.shared.extension.Content;

@Presenter
public class LOCPresenter extends BaseClientPresenter<LOCView> implements LOCContext {

    private TradeFinanceContext tradeFinanceContext;

    @InjectContext(extensionPoint = TradeFinanceExtensionPoint.class)
    public void contributeToTradeFinanceModule(TradeFinanceContext context) {
        tradeFinanceContext = context;
        context.getAppModuleContext().addModule(new AppModuleContext.AppModule() {
            @Override
            public String getModuleName() {
                return "Letter of credit";
            }

            @Override
            public String getHistoryToken() {
                return "letter-of-credit";
            }

            @Override
            public String getPath() {
                return LOCContext.PATH;
            }

            @Override
            public boolean hasContent() {
                return true;
            }

            @Override
            public Content getContent() {
                return view.getContent();
            }

            @Override
            public String getModuleIconName() {
                return "payment";
            }
        });
        applyContributions(LOCExtensionPoint.class, () -> this);
    }

    @Override
    public TradeFinanceContext getTradeFinanceContext() {
        return tradeFinanceContext;
    }
}