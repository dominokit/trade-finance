package com.progressoft.corpay.loc.client;

import com.google.gwt.core.client.EntryPoint;
import org.dominokit.domino.api.client.ModuleConfigurator;
import org.dominokit.domino.api.client.annotations.ClientModule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ClientModule(name="LOC")
public class LOCClientModule implements EntryPoint {

	private static final Logger LOGGER = LoggerFactory.getLogger(LOCClientModule.class);

	public void onModuleLoad() {
		LOGGER.info("Initializing LOC frontend module ...");
		new ModuleConfigurator().configureModule(new LOCModuleConfiguration());
	}
}
