package com.progressoft.corpay.loc.client.contributions;

import com.progressoft.corpay.loc.client.presenters.LOCPresenterCommand;
import com.progressoft.corpay.tradefinance.shared.extension.TradeFinanceExtensionPoint;
import javax.annotation.Generated;
import org.dominokit.domino.api.client.annotations.Contribute;
import org.dominokit.domino.api.shared.extension.Contribution;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.inject.InjectContextProcessor")
@Contribute
public class LOCPresenterContributionToTradeFinanceExtensionPoint implements Contribution<TradeFinanceExtensionPoint> {
  @Override
  public void contribute(TradeFinanceExtensionPoint extensionPoint) {
    new LOCPresenterCommand().onPresenterReady(presenter -> presenter.contributeToTradeFinanceModule(extensionPoint.context())).send();
  }
}
