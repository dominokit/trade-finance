package com.progressoft.corpay.loc.client.requests;

import com.progressoft.corpay.loc.shared.request.LOCRequest;
import com.progressoft.corpay.loc.shared.response.LOCResponse;
import javax.annotation.Generated;
import org.dominokit.domino.api.client.annotations.Path;
import org.dominokit.domino.api.client.annotations.Request;
import org.dominokit.domino.api.client.request.Response;
import org.dominokit.domino.api.client.request.ServerRequest;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.group.RequestFactoryProcessor")
public class LOCRequestsFactory implements LOCRequests {
  public static final LOCRequestsFactory INSTANCE = new LOCRequestsFactory();

  @Override
  public Response<LOCResponse> request(LOCRequest request) {
    return new LOCRequests_request(request);
  }

  @Request
  @Path("LOCRequest")
  public class LOCRequests_request extends ServerRequest<LOCRequest, LOCResponse> {
    LOCRequests_request(LOCRequest request) {
      super(request);
    }
  }
}
