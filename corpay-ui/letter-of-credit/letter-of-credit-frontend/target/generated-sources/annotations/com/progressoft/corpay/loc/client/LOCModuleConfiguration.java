package com.progressoft.corpay.loc.client;

import com.progressoft.corpay.loc.client.contributions.LOCPresenterContributionToTradeFinanceExtensionPoint;
import com.progressoft.corpay.loc.client.locexport.presenters.LOCExportPresenter;
import com.progressoft.corpay.loc.client.locexport.presenters.LOCExportPresenterCommand;
import com.progressoft.corpay.loc.client.locimport.contributions.LOCImportPresenterContributionToLOCExtensionPoint;
import com.progressoft.corpay.loc.client.locimport.presenters.LOCImportPresenter;
import com.progressoft.corpay.loc.client.locimport.presenters.LOCImportPresenterCommand;
import com.progressoft.corpay.loc.client.presenters.LOCPresenter;
import com.progressoft.corpay.loc.client.presenters.LOCPresenterCommand;
import com.progressoft.corpay.loc.client.requests.LOCRequestsFactory;
import com.progressoft.corpay.loc.client.requests.LOCRequests_requestSender;
import com.progressoft.corpay.loc.shared.extension.LOCExtensionPoint;
import com.progressoft.corpay.tradefinance.shared.extension.TradeFinanceExtensionPoint;
import javax.annotation.Generated;
import org.dominokit.domino.api.client.ModuleConfiguration;
import org.dominokit.domino.api.client.extension.ContributionsRegistry;
import org.dominokit.domino.api.client.mvp.PresenterRegistry;
import org.dominokit.domino.api.client.mvp.presenter.LazyPresenterLoader;
import org.dominokit.domino.api.client.mvp.presenter.Presentable;
import org.dominokit.domino.api.client.request.CommandRegistry;
import org.dominokit.domino.api.client.request.LazyRequestRestSenderLoader;
import org.dominokit.domino.api.client.request.RequestRestSender;
import org.dominokit.domino.api.client.request.RequestRestSendersRegistry;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.module.client.ClientModuleAnnotationProcessor")
public class LOCModuleConfiguration implements ModuleConfiguration {
  @Override
  public void registerPresenters(PresenterRegistry registry) {
    registry.registerPresenter(new LazyPresenterLoader(LOCExportPresenter.class.getCanonicalName(), LOCExportPresenter.class.getCanonicalName()) {
      @Override
      protected Presentable make() {
        return new LOCExportPresenter();
      }
    });
    registry.registerPresenter(new LazyPresenterLoader(LOCImportPresenter.class.getCanonicalName(), LOCImportPresenter.class.getCanonicalName()) {
      @Override
      protected Presentable make() {
        return new LOCImportPresenter();
      }
    });
    registry.registerPresenter(new LazyPresenterLoader(LOCPresenter.class.getCanonicalName(), LOCPresenter.class.getCanonicalName()) {
      @Override
      protected Presentable make() {
        return new LOCPresenter();
      }
    });
  }

  @Override
  public void registerRequests(CommandRegistry registry) {
    registry.registerCommand(LOCExportPresenterCommand.class.getCanonicalName(), LOCExportPresenter.class.getCanonicalName());
    registry.registerCommand(LOCImportPresenterCommand.class.getCanonicalName(), LOCImportPresenter.class.getCanonicalName());
    registry.registerCommand(LOCPresenterCommand.class.getCanonicalName(), LOCPresenter.class.getCanonicalName());
  }

  @Override
  public void registerContributions(ContributionsRegistry registry) {
    registry.registerContribution(TradeFinanceExtensionPoint.class, new LOCPresenterContributionToTradeFinanceExtensionPoint());
    registry.registerContribution(LOCExtensionPoint.class, new LOCImportPresenterContributionToLOCExtensionPoint());
  }

  @Override
  public void registerRequestRestSenders(RequestRestSendersRegistry registry) {
    registry.registerRequestRestSender(LOCRequestsFactory.LOCRequests_request.class.getCanonicalName(), new LazyRequestRestSenderLoader() {
      @Override
      protected RequestRestSender make() {
        return new LOCRequests_requestSender();
      }
    });
  }
}
