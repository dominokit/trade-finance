package com.progressoft.corpay.loc.client.locimport.contributions;

import com.progressoft.corpay.loc.client.locimport.presenters.LOCImportPresenterCommand;
import com.progressoft.corpay.loc.shared.extension.LOCExtensionPoint;
import javax.annotation.Generated;
import org.dominokit.domino.api.client.annotations.Contribute;
import org.dominokit.domino.api.shared.extension.Contribution;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.inject.InjectContextProcessor")
@Contribute
public class LOCImportPresenterContributionToLOCExtensionPoint implements Contribution<LOCExtensionPoint> {
  @Override
  public void contribute(LOCExtensionPoint extensionPoint) {
    new LOCImportPresenterCommand().onPresenterReady(presenter -> presenter.contributeToLOCModule(extensionPoint.context())).send();
  }
}
