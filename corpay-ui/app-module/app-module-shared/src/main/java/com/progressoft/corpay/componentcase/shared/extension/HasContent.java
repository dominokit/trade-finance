package com.progressoft.corpay.componentcase.shared.extension;

import org.dominokit.domino.api.shared.extension.Content;

@FunctionalInterface
public interface HasContent {
    Content getContent();
}
