package com.progressoft.corpay.componentcase.shared.extension;

import org.dominokit.domino.api.shared.extension.Content;
import org.dominokit.domino.api.shared.extension.Context;

public interface AppModuleContext extends Context {

    void addModule(AppModule appModule);

    interface AppModule {
        String getModuleName();

        String getHistoryToken();

        default String getModuleIconName() {
            return "";
        }

        default Content getContent() {
            return null;
        }

        String getPath();

        default boolean hasContent() {
            return false;
        }
    }

}
