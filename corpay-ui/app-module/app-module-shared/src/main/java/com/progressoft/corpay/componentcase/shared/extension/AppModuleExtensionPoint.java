package com.progressoft.corpay.componentcase.shared.extension;

import org.dominokit.domino.api.shared.extension.ExtensionPoint;

public interface AppModuleExtensionPoint extends ExtensionPoint<AppModuleContext>{
}
