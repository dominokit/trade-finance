package com.progressoft.corpay.componentcase.client;

import com.progressoft.corpay.componentcase.client.presenters.AppModulePresenter;
import com.progressoft.corpay.componentcase.client.ui.views.AppModuleViewImpl;
import javax.annotation.Generated;
import org.dominokit.domino.api.client.ModuleConfiguration;
import org.dominokit.domino.api.client.mvp.ViewRegistry;
import org.dominokit.domino.api.client.mvp.view.LazyViewLoader;
import org.dominokit.domino.api.client.mvp.view.View;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.module.client.ClientModuleAnnotationProcessor")
public class ComponentCaseUIModuleConfiguration implements ModuleConfiguration {
  @Override
  public void registerViews(ViewRegistry registry) {
    registry.registerView(new LazyViewLoader(AppModulePresenter.class.getCanonicalName()) {
      @Override
      protected View make() {
        return new AppModuleViewImpl();
      }
    });
  }
}
