package com.progressoft.corpay.componentcase.client.ui.views;

import com.progressoft.corpay.componentcase.client.presenters.AppModulePresenter;
import com.progressoft.corpay.componentcase.client.views.AppModuleView;
import com.progressoft.corpay.componentcase.shared.extension.AppModuleContext;
import com.progressoft.corpay.layout.shared.IsLayout;
import elemental2.dom.DomGlobal;
import elemental2.dom.HTMLElement;
import jsinterop.base.Js;
import org.dominokit.domino.api.client.annotations.UiView;
import org.dominokit.domino.api.shared.extension.Content;
import org.dominokit.domino.ui.icons.Icon;
import org.dominokit.domino.ui.menu.Menu;
import org.dominokit.domino.ui.menu.MenuItem;
import org.dominokit.domino.ui.style.Style;
import org.dominokit.domino.ui.tabs.Tab;
import org.dominokit.domino.ui.tabs.TabsPanel;
import org.dominokit.domino.ui.utils.ElementUtil;

import static java.util.Objects.nonNull;

@UiView(presentable = AppModulePresenter.class)
public class AppModuleViewImpl implements AppModuleView {

    private IsLayout layout;
    private TabsPanel tabsPanel = Style.of(TabsPanel.create()).setMarginLeft("20px").get();
    private AppModuleUiHandlers uiHandlers;

    @Override
    public void init(IsLayout layout) {
        this.layout = layout;
        Js.<HTMLElement>uncheckedCast(layout.getProductNavigation().getPrimaryNavigationBar().get())
                .appendChild(tabsPanel.asElement());
    }

    @Override
    public void clear() {
        ElementUtil.clear(Js.cast(layout.getContentPanel().get()));
        ElementUtil.clear(Js.cast(layout.getLeftPanel().get()));
    }

    @Override
    public void scrollTop() {
        DomGlobal.document.body.scrollTop = 0;
        DomGlobal.document.documentElement.scrollTop = 0;
    }

    @Override
    public void showContent(Content content) {
        if (nonNull(content)) {
            ElementUtil.clear(Js.cast(layout.getContentPanel().get()));
            Js.<HTMLElement>cast(layout.getContentPanel().get()).appendChild(Js.cast(content.get()));
        }
    }

    @Override
    public Module addMainModule(AppModuleContext.AppModule appModule) {
        Tab tab = Tab.create(Icon.create(appModule.getModuleIconName()), appModule.getModuleName());
        Menu menu = Menu.create(appModule.getModuleName());
        tab.getClickableElement().addEventListener("click", evt -> {
            this.uiHandlers.onAppModuleSelected(appModule);
        });
        tabsPanel.addTab(tab);
        return new MainModule(menu, appModule);
    }

    @Override
    public void showAppModuleMenu(Content menuContent) {
        Js.<HTMLElement>uncheckedCast(layout.getLeftPanel().get())
                .appendChild(Js.uncheckedCast(menuContent.get()));
    }

    @Override
    public void addSubModule(AppModuleContext.AppModule appModule) {

    }

    @Override
    public void setUiHandlers(AppModuleUiHandlers uiHandlers) {
        this.uiHandlers = uiHandlers;
    }

    private class MainModule implements Module {

        private Menu menu;
        private AppModuleContext.AppModule appModule;

        public MainModule(Menu menu, AppModuleContext.AppModule appModule) {
            this.menu = menu;
            this.appModule = appModule;
        }

        @Override
        public CanAddMenuItem getMenu() {
            return new CanAddMenuItem() {
                @Override
                public CanAddMenuItem addMenuItem(String title) {
                    MenuItem menuItem = menu.addMenuItem(title, Icon.create(appModule.getModuleIconName()));
                    return new SubModuleMenuItem(menuItem);
                }

                @Override
                public CanAddMenuItem addMenuItem(String title,
                                                  MenuSelectionHandler selectionHandler) {
                    MenuItem menuItem = menu.addMenuItem(title, Icon.create(appModule.getModuleIconName()));
                    menuItem.getClickableElement().addEventListener("click", evt -> {
                        selectionHandler.onMenuSelected();
                    });
                    return new SubModuleMenuItem(menuItem);
                }

                @Override
                public Content getContent() {
                    return () -> menu.asElement();
                }
            };
        }

        @Override
        public AppModuleContext.AppModule getAppModule() {
            return appModule;
        }
    }

    private class SubModuleMenuItem implements CanAddMenuItem {

        private MenuItem parent;

        public SubModuleMenuItem(MenuItem parent) {
            this.parent = parent;
        }

        @Override
        public CanAddMenuItem addMenuItem(String title) {
            MenuItem menuItem = parent.addMenuItem(title);
            return new SubModuleMenuItem(menuItem);
        }

        @Override
        public CanAddMenuItem addMenuItem(String title, MenuSelectionHandler selectionHandler) {
            MenuItem menuItem = parent.addMenuItem(title);
            menuItem.getClickableElement().addEventListener("click", evt -> {
                selectionHandler.onMenuSelected();
            });
            return new SubModuleMenuItem(menuItem);
        }

        @Override
        public Content getContent() {
            return () -> parent;
        }
    }
}