package com.progressoft.corpay.componentcase.client.contributions;

import com.progressoft.corpay.componentcase.client.presenters.AppModulePresenterCommand;
import com.progressoft.corpay.layout.shared.extension.AppLayoutExtensionPoint;
import javax.annotation.Generated;
import org.dominokit.domino.api.client.annotations.Contribute;
import org.dominokit.domino.api.shared.extension.Contribution;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.inject.InjectContextProcessor")
@Contribute
public class AppModulePresenterContributionToAppLayoutExtensionPoint implements Contribution<AppLayoutExtensionPoint> {
  @Override
  public void contribute(AppLayoutExtensionPoint extensionPoint) {
    new AppModulePresenterCommand().onPresenterReady(presenter -> presenter.contributeToLayoutModule(extensionPoint.context())).send();
  }
}
