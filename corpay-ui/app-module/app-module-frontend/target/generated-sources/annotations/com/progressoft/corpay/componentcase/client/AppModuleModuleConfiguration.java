package com.progressoft.corpay.componentcase.client;

import com.progressoft.corpay.componentcase.client.contributions.AppModulePresenterContributionToAppLayoutExtensionPoint;
import com.progressoft.corpay.componentcase.client.presenters.AppModulePresenter;
import com.progressoft.corpay.componentcase.client.presenters.AppModulePresenterCommand;
import com.progressoft.corpay.layout.shared.extension.AppLayoutExtensionPoint;
import javax.annotation.Generated;
import org.dominokit.domino.api.client.ModuleConfiguration;
import org.dominokit.domino.api.client.extension.ContributionsRegistry;
import org.dominokit.domino.api.client.mvp.PresenterRegistry;
import org.dominokit.domino.api.client.mvp.presenter.LazyPresenterLoader;
import org.dominokit.domino.api.client.mvp.presenter.Presentable;
import org.dominokit.domino.api.client.request.CommandRegistry;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.module.client.ClientModuleAnnotationProcessor")
public class AppModuleModuleConfiguration implements ModuleConfiguration {
  @Override
  public void registerPresenters(PresenterRegistry registry) {
    registry.registerPresenter(new LazyPresenterLoader(AppModulePresenter.class.getCanonicalName(), AppModulePresenter.class.getCanonicalName()) {
      @Override
      protected Presentable make() {
        return new AppModulePresenter();
      }
    });
  }

  @Override
  public void registerRequests(CommandRegistry registry) {
    registry.registerCommand(AppModulePresenterCommand.class.getCanonicalName(), AppModulePresenter.class.getCanonicalName());
  }

  @Override
  public void registerContributions(ContributionsRegistry registry) {
    registry.registerContribution(AppLayoutExtensionPoint.class, new AppModulePresenterContributionToAppLayoutExtensionPoint());
  }
}
