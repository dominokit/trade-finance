package com.progressoft.corpay.componentcase.client;

import com.google.gwt.core.client.EntryPoint;
import org.dominokit.domino.api.client.ModuleConfigurator;
import org.dominokit.domino.api.client.annotations.ClientModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ClientModule(name="AppModule")
public class AppModuleClientModule implements EntryPoint {

	private static final Logger LOGGER = LoggerFactory.getLogger(AppModuleClientModule.class);

	public void onModuleLoad() {
		LOGGER.info("Initializing AppModule frontend module ...");
		new ModuleConfigurator().configureModule(new AppModuleModuleConfiguration());
	}
}
