package com.progressoft.corpay.componentcase.client.views;

import com.progressoft.corpay.componentcase.shared.extension.AppModuleContext;
import com.progressoft.corpay.layout.shared.IsLayout;
import org.dominokit.domino.api.client.mvp.view.HasUiHandlers;
import org.dominokit.domino.api.client.mvp.view.UiHandlers;
import org.dominokit.domino.api.client.mvp.view.View;
import org.dominokit.domino.api.shared.extension.Content;

public interface AppModuleView extends View, HasUiHandlers<AppModuleView.AppModuleUiHandlers> {
    void init(IsLayout layout);

    void clear();

    void scrollTop();

    void showContent(Content content);

    Module addMainModule(AppModuleContext.AppModule appModule);

    void showAppModuleMenu(Content content);

    void addSubModule(AppModuleContext.AppModule appModule);

    interface AppModuleUiHandlers extends UiHandlers {
        void onAppModuleSelected(AppModuleContext.AppModule appModule);
    }

    interface Module {
        CanAddMenuItem getMenu();
        AppModuleContext.AppModule getAppModule();
    }

    interface CanAddMenuItem {
        CanAddMenuItem addMenuItem(String title);

        CanAddMenuItem addMenuItem(String title, MenuSelectionHandler selectionHandler);

        Content getContent();
    }

    @FunctionalInterface
    interface MenuSelectionHandler {
        void onMenuSelected();
    }
}
