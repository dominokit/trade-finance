package com.progressoft.corpay.componentcase.client.presenters;

import com.progressoft.corpay.componentcase.client.views.AppModuleView;
import com.progressoft.corpay.componentcase.shared.extension.AppModuleContext;
import com.progressoft.corpay.componentcase.shared.extension.AppModuleExtensionPoint;
import com.progressoft.corpay.layout.shared.extension.AppLayoutContext;
import com.progressoft.corpay.layout.shared.extension.AppLayoutExtensionPoint;
import org.dominokit.domino.api.client.annotations.InjectContext;
import org.dominokit.domino.api.client.annotations.Presenter;
import org.dominokit.domino.api.client.mvp.presenter.BaseClientPresenter;
import org.dominokit.domino.api.shared.history.HistoryToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.progressoft.corpay.componentcase.client.views.AppModuleView.AppModuleUiHandlers;
import static com.progressoft.corpay.componentcase.client.views.AppModuleView.Module;

@Presenter
public class AppModulePresenter extends BaseClientPresenter<AppModuleView> implements AppModuleContext,
        AppModuleUiHandlers {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppModulePresenter.class);

    private AppLayoutContext appLayoutContext;
    private AppModule currentModule;
    private Map<String, Module> modules = new HashMap<>();
    private Map<String, String> modulesPaths = new HashMap<>();

    @Override
    protected void initView(AppModuleView view) {
        view.setUiHandlers(this);
    }

    @InjectContext(extensionPoint = AppLayoutExtensionPoint.class)
    public void contributeToLayoutModule(AppLayoutContext context) {
        this.appLayoutContext = context;
        view.init(appLayoutContext.getLayout());
        applyContributions(AppModuleExtensionPoint.class, () -> AppModulePresenter.this);
    }

    @Override
    public void addModule(AppModule appModule) {
        int pathsCount = appModule.getPath().split("/").length;

        if (pathsCount <= 1) {
            addMainModule(appModule);
            history()
                    .listen(token -> token.path().endsWith(appModule.getHistoryToken()),
                            state -> showAppModule(appModule))
                    .onDirectUrl(state -> showAppModule(appModule));
        } else {
            addSubModule(appModule);
        }
    }

    private void addSubModule(AppModule appModule) {
        String path = appModule.getPath();
        int lastIndex = path.lastIndexOf('/');
        String rootPath = "";
        if (lastIndex == -1)
            rootPath = path;
        else {
            rootPath = path.substring(0, lastIndex);
        }
        String moduleName = modulesPaths.get(rootPath);
        Module parent = modules.get(moduleName);
        AppModuleView.CanAddMenuItem subModuleMenu = parent.getMenu().addMenuItem(appModule.getModuleName(), () -> {
            onSubModuleSelected(appModule, parent);
        });
        registerModule(appModule, new Module() {
            @Override
            public AppModuleView.CanAddMenuItem getMenu() {
                return subModuleMenu;
            }

            @Override
            public AppModule getAppModule() {
                return appModule;
            }
        });

        history()
                .listen(token -> token.path().endsWith(appModule.getHistoryToken()),
                        state -> onSubModuleSelected(appModule, parent))
                .onDirectUrl(state -> onSubModuleSelected(appModule, parent));
    }

    private void onSubModuleSelected(AppModule appModule, Module parent) {
        HistoryToken historyToken = history().currentToken();
        List<String> paths = new ArrayList<>(historyToken.paths());
        if(!historyToken.endsWithPath(appModule.getHistoryToken())) {
            historyToken.clearPaths();
            for (String path: paths) {
                historyToken.appendPath(path);
                if (parent.getAppModule().getHistoryToken().equals(path)) {
                    break;
                }
            }
            String newToken = historyToken.appendPath(appModule.getHistoryToken())
                    .value();
            history().pushState(newToken);
            history().pushState(historyToken.value());

            if (appModule.hasContent())
                view.showContent(appModule.getContent());
        }
    }

    private void addMainModule(AppModule appModule) {
        Module module = view.addMainModule(appModule);
        registerModule(appModule, module);
    }

    private void registerModule(AppModule appModule, Module module) {
        modules.put(appModule.getModuleName(), module);
        modulesPaths.put(appModule.getPath(), appModule.getModuleName());
    }

    @Override
    public void onAppModuleSelected(AppModule appModule) {
        history().pushState(history().currentToken().replaceAllPaths(appModule.getHistoryToken()).value());
        showAppModule(appModule);
    }

    private void showAppModule(AppModule appModule) {
        Module module = modules.get(appModule.getModuleName());
        view.clear();
        if (appModule.hasContent())
            view.showContent(appModule.getContent());
        view.showAppModuleMenu(module.getMenu().getContent());
    }
}