package com.progressoft.corpay.guarantee.client;

import com.progressoft.corpay.guarantee.client.bidbonds.presenters.BidBondsPresenter;
import com.progressoft.corpay.guarantee.client.bidbonds.ui.views.BidBondsViewImpl;
import com.progressoft.corpay.guarantee.client.presenters.LOGPresenter;
import com.progressoft.corpay.guarantee.client.ui.views.LOGViewImpl;
import javax.annotation.Generated;
import org.dominokit.domino.api.client.ModuleConfiguration;
import org.dominokit.domino.api.client.mvp.ViewRegistry;
import org.dominokit.domino.api.client.mvp.view.LazyViewLoader;
import org.dominokit.domino.api.client.mvp.view.View;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.module.client.ClientModuleAnnotationProcessor")
public class LOGUIModuleConfiguration implements ModuleConfiguration {
  @Override
  public void registerViews(ViewRegistry registry) {
    registry.registerView(new LazyViewLoader(BidBondsPresenter.class.getCanonicalName()) {
      @Override
      protected View make() {
        return new BidBondsViewImpl();
      }
    });
    registry.registerView(new LazyViewLoader(LOGPresenter.class.getCanonicalName()) {
      @Override
      protected View make() {
        return new LOGViewImpl();
      }
    });
  }
}
