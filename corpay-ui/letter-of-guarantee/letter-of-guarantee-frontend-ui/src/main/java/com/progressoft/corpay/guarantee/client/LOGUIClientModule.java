package com.progressoft.corpay.guarantee.client;

import com.google.gwt.core.client.EntryPoint;
import org.dominokit.domino.api.client.ModuleConfigurator;
import org.dominokit.domino.api.client.annotations.ClientModule;
import com.progressoft.corpay.guarantee.client.ui.views.Bundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.corpay.guarantee.client.ui.views.LOGBundle;

@ClientModule(name="LOGUI")
public class LOGUIClientModule implements EntryPoint {

	private static final Logger LOGGER = LoggerFactory.getLogger(LOGUIClientModule.class);

	public void onModuleLoad() {
		LOGGER.info("Initializing LOG frontend UI module ...");
		Bundle.INSTANCE.style().ensureInjected();
		new ModuleConfigurator().configureModule(new LOGUIModuleConfiguration());
	}
}
