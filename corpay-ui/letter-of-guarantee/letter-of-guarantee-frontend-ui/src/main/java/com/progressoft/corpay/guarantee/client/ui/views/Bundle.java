package com.progressoft.corpay.guarantee.client.ui.views;

import com.google.gwt.core.client.GWT;

public class Bundle {

    public static final LOGBundle INSTANCE= GWT.create(LOGBundle.class);

    private Bundle() {
    }
}
