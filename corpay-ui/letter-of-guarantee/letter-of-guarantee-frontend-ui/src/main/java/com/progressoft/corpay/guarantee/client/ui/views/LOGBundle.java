package com.progressoft.corpay.guarantee.client.ui.views;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;

public interface LOGBundle extends ClientBundle{

    interface Style extends CssResource {
        String LOG();
    }

    @Source("css/LOG.gss")
    Style style();
}