package com.progressoft.corpay.guarantee.client.bidbonds.ui.views;

import com.progressoft.corpay.guarantee.client.bidbonds.presenters.BidBondsPresenter;
import com.progressoft.corpay.guarantee.client.bidbonds.views.BidBondsView;
import org.dominokit.domino.api.client.annotations.UiView;
import org.dominokit.domino.api.shared.extension.Content;
import org.dominokit.domino.ui.cards.Card;

@UiView(presentable = BidBondsPresenter.class)
public class BidBondsViewImpl implements BidBondsView {
    @Override
    public Content getContent() {
        return () -> Card.create("Bid bonds").asElement();
    }
}
