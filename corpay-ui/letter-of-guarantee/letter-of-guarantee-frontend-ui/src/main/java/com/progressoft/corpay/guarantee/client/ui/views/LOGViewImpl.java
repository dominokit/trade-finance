package com.progressoft.corpay.guarantee.client.ui.views;

import com.progressoft.corpay.guarantee.client.presenters.LOGPresenter;
import com.progressoft.corpay.guarantee.client.views.LOGView;
import org.dominokit.domino.api.client.annotations.UiView;
import org.dominokit.domino.api.shared.extension.Content;
import org.dominokit.domino.ui.cards.Card;

@UiView(presentable = LOGPresenter.class)
public class LOGViewImpl implements LOGView {

    @Override
    public Content getContent() {
        return () -> Card.create("Letter of guarantee").asElement();
    }
}