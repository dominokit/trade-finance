package com.progressoft.corpay.guarantee.client.requests;

import com.progressoft.corpay.guarantee.shared.request.LOGRequest;
import com.progressoft.corpay.guarantee.shared.response.LOGResponse;
import javax.annotation.Generated;
import org.dominokit.domino.api.client.annotations.Path;
import org.dominokit.domino.api.client.annotations.Request;
import org.dominokit.domino.api.client.request.Response;
import org.dominokit.domino.api.client.request.ServerRequest;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.group.RequestFactoryProcessor")
public class LOGRequestsFactory implements LOGRequests {
  public static final LOGRequestsFactory INSTANCE = new LOGRequestsFactory();

  @Override
  public Response<LOGResponse> request(LOGRequest request) {
    return new LOGRequests_request(request);
  }

  @Request
  @Path("LOGRequest")
  public class LOGRequests_request extends ServerRequest<LOGRequest, LOGResponse> {
    LOGRequests_request(LOGRequest request) {
      super(request);
    }
  }
}
