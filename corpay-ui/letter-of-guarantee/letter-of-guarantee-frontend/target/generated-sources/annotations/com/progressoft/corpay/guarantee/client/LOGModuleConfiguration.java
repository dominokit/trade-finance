package com.progressoft.corpay.guarantee.client;

import com.progressoft.corpay.guarantee.client.bidbonds.presenters.BidBondsPresenter;
import com.progressoft.corpay.guarantee.client.bidbonds.presenters.BidBondsPresenterCommand;
import com.progressoft.corpay.guarantee.client.contributions.LOGPresenterContributionToTradeFinanceExtensionPoint;
import com.progressoft.corpay.guarantee.client.presenters.LOGPresenter;
import com.progressoft.corpay.guarantee.client.presenters.LOGPresenterCommand;
import com.progressoft.corpay.guarantee.client.requests.LOGRequestsFactory;
import com.progressoft.corpay.guarantee.client.requests.LOGRequests_requestSender;
import com.progressoft.corpay.tradefinance.shared.extension.TradeFinanceExtensionPoint;
import javax.annotation.Generated;
import org.dominokit.domino.api.client.ModuleConfiguration;
import org.dominokit.domino.api.client.extension.ContributionsRegistry;
import org.dominokit.domino.api.client.mvp.PresenterRegistry;
import org.dominokit.domino.api.client.mvp.presenter.LazyPresenterLoader;
import org.dominokit.domino.api.client.mvp.presenter.Presentable;
import org.dominokit.domino.api.client.request.CommandRegistry;
import org.dominokit.domino.api.client.request.LazyRequestRestSenderLoader;
import org.dominokit.domino.api.client.request.RequestRestSender;
import org.dominokit.domino.api.client.request.RequestRestSendersRegistry;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.module.client.ClientModuleAnnotationProcessor")
public class LOGModuleConfiguration implements ModuleConfiguration {
  @Override
  public void registerPresenters(PresenterRegistry registry) {
    registry.registerPresenter(new LazyPresenterLoader(BidBondsPresenter.class.getCanonicalName(), BidBondsPresenter.class.getCanonicalName()) {
      @Override
      protected Presentable make() {
        return new BidBondsPresenter();
      }
    });
    registry.registerPresenter(new LazyPresenterLoader(LOGPresenter.class.getCanonicalName(), LOGPresenter.class.getCanonicalName()) {
      @Override
      protected Presentable make() {
        return new LOGPresenter();
      }
    });
  }

  @Override
  public void registerRequests(CommandRegistry registry) {
    registry.registerCommand(BidBondsPresenterCommand.class.getCanonicalName(), BidBondsPresenter.class.getCanonicalName());
    registry.registerCommand(LOGPresenterCommand.class.getCanonicalName(), LOGPresenter.class.getCanonicalName());
  }

  @Override
  public void registerContributions(ContributionsRegistry registry) {
    registry.registerContribution(TradeFinanceExtensionPoint.class, new LOGPresenterContributionToTradeFinanceExtensionPoint());
  }

  @Override
  public void registerRequestRestSenders(RequestRestSendersRegistry registry) {
    registry.registerRequestRestSender(LOGRequestsFactory.LOGRequests_request.class.getCanonicalName(), new LazyRequestRestSenderLoader() {
      @Override
      protected RequestRestSender make() {
        return new LOGRequests_requestSender();
      }
    });
  }
}
