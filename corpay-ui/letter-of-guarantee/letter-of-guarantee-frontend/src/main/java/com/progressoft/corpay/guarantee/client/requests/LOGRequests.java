package com.progressoft.corpay.guarantee.client.requests;

import org.dominokit.domino.api.client.annotations.Path;
import org.dominokit.domino.api.client.annotations.RequestFactory;
import org.dominokit.domino.api.client.request.Response;
import com.progressoft.corpay.guarantee.shared.response.LOGResponse;
import com.progressoft.corpay.guarantee.shared.request.LOGRequest;

@RequestFactory
public interface LOGRequests {
    @Path("LOGRequest")
    Response<LOGResponse> request(LOGRequest request);
}
