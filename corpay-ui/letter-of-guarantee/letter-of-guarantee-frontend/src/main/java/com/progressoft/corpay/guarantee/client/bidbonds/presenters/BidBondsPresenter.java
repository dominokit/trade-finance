package com.progressoft.corpay.guarantee.client.bidbonds.presenters;

import com.progressoft.corpay.guarantee.client.bidbonds.views.BidBondsView;
import org.dominokit.domino.api.client.annotations.Presenter;
import org.dominokit.domino.api.client.mvp.presenter.BaseClientPresenter;

@Presenter
public class BidBondsPresenter extends BaseClientPresenter<BidBondsView> {

}
