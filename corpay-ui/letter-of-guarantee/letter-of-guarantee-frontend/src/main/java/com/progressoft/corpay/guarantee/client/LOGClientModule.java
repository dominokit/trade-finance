package com.progressoft.corpay.guarantee.client;

import com.google.gwt.core.client.EntryPoint;
import org.dominokit.domino.api.client.ModuleConfigurator;
import org.dominokit.domino.api.client.annotations.ClientModule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ClientModule(name="LOG")
public class LOGClientModule implements EntryPoint {

	private static final Logger LOGGER = LoggerFactory.getLogger(LOGClientModule.class);

	public void onModuleLoad() {
		LOGGER.info("Initializing LOG frontend module ...");
		new ModuleConfigurator().configureModule(new LOGModuleConfiguration());
	}
}
