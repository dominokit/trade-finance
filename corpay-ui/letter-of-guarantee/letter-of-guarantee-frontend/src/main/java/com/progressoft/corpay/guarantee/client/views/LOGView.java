package com.progressoft.corpay.guarantee.client.views;

import com.progressoft.corpay.componentcase.shared.extension.HasContent;
import org.dominokit.domino.api.client.mvp.view.View;
import org.dominokit.domino.api.shared.extension.Content;

public interface LOGView extends View, HasContent {
}