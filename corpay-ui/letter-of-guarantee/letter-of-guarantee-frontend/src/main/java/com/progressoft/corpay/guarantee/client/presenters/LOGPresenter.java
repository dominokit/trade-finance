package com.progressoft.corpay.guarantee.client.presenters;

import com.progressoft.corpay.componentcase.shared.extension.AppModuleContext;
import com.progressoft.corpay.guarantee.client.views.LOGView;
import com.progressoft.corpay.tradefinance.shared.extension.TradeFinanceContext;
import com.progressoft.corpay.tradefinance.shared.extension.TradeFinanceExtensionPoint;
import org.dominokit.domino.api.client.annotations.InjectContext;
import org.dominokit.domino.api.client.annotations.Presenter;
import org.dominokit.domino.api.client.mvp.presenter.BaseClientPresenter;
import org.dominokit.domino.api.shared.extension.Content;

@Presenter
public class LOGPresenter extends BaseClientPresenter<LOGView> {

    @InjectContext(extensionPoint = TradeFinanceExtensionPoint.class)
    public void contributeToTradeFinanceModule(TradeFinanceContext context) {
        context.getAppModuleContext().addModule(new AppModuleContext.AppModule() {
            @Override
            public String getModuleName() {
                return "Letter of guarantee";
            }

            @Override
            public String getHistoryToken() {
                return "letter-of-guarantee";
            }

            @Override
            public String getPath() {
                return TradeFinanceContext.PATH + "/" + getModuleName();
            }

            @Override
            public boolean hasContent() {
                return true;
            }

            @Override
            public Content getContent() {
                return view.getContent();
            }

            @Override
            public String getModuleIconName() {
                return "turned_out_in";
            }
        });
    }
}