package com.progressoft.corpay.guarantee.client.bidbonds.views;

import com.progressoft.corpay.componentcase.shared.extension.HasContent;
import org.dominokit.domino.api.client.mvp.view.View;

public interface BidBondsView extends View, HasContent {
}
