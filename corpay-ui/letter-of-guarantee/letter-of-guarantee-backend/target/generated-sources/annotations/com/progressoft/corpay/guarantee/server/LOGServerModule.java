package com.progressoft.corpay.guarantee.server;

import com.google.auto.service.AutoService;
import com.progressoft.corpay.guarantee.server.handlers.LOGHandler;
import com.progressoft.corpay.guarantee.server.handlers.LOGHandlerEndpointHandler;
import java.util.function.Supplier;
import javax.annotation.Generated;
import org.dominokit.domino.api.server.config.ServerModuleConfiguration;
import org.dominokit.domino.api.server.endpoint.EndpointsRegistry;
import org.dominokit.domino.api.server.handler.HandlerRegistry;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.server.ServerModuleAnnotationProcessor")
@AutoService(ServerModuleConfiguration.class)
public class LOGServerModule implements ServerModuleConfiguration {
  @Override
  public void registerHandlers(HandlerRegistry registry) {
    registry.registerHandler("LOGRequest",new LOGHandler());
  }

  @Override
  public void registerEndpoints(EndpointsRegistry registry) {
    registry.registerEndpoint("LOGRequest", new Supplier<LOGHandlerEndpointHandler>() {
      @Override
      public LOGHandlerEndpointHandler get() {
        return new LOGHandlerEndpointHandler();
      }
    });
  }
}
