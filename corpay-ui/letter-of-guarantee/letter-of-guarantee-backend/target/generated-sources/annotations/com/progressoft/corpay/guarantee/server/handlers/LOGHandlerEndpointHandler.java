package com.progressoft.corpay.guarantee.server.handlers;

import com.progressoft.corpay.guarantee.shared.request.LOGRequest;
import com.progressoft.corpay.guarantee.shared.response.LOGResponse;
import javax.annotation.Generated;
import org.dominokit.domino.api.server.endpoint.AbstractEndpoint;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.server.EndpointsProcessor")
public class LOGHandlerEndpointHandler extends AbstractEndpoint<LOGRequest, LOGResponse> {
  @Override
  protected LOGRequest makeNewRequest() {
    return new LOGRequest();
  }

  @Override
  protected Class<LOGRequest> getRequestClass() {
    return LOGRequest.class;
  }
}
