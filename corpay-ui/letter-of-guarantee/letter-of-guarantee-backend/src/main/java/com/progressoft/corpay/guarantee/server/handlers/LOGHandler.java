package com.progressoft.corpay.guarantee.server.handlers;

import org.dominokit.domino.api.server.handler.Handler;
import org.dominokit.domino.api.server.handler.RequestHandler;
import org.dominokit.domino.api.server.context.ExecutionContext;
import com.progressoft.corpay.guarantee.shared.response.LOGResponse;
import com.progressoft.corpay.guarantee.shared.request.LOGRequest;

import java.util.logging.Logger;

@Handler("LOGRequest")
public class LOGHandler implements RequestHandler<LOGRequest, LOGResponse> {
    private static final Logger LOGGER= Logger.getLogger(LOGHandler.class.getName());
    @Override
    public void handleRequest(ExecutionContext<LOGRequest, LOGResponse> executionContext) {
        LOGGER.info("message recieved from client : "+executionContext.getRequestBean().getMessage());
        executionContext.end(new LOGResponse("Server message"));
    }
}
