package com.progressoft.corpay.guarantee.shared.request;

import org.dominokit.domino.api.shared.request.RequestBean;

public class LOGRequest implements RequestBean {

    private String message;

    public LOGRequest() {
    }

    public LOGRequest(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
