package com.progressoft.corpay.guarantee.shared.response;

import org.dominokit.domino.api.shared.request.ResponseBean;

public class LOGResponse implements ResponseBean {

    private String serverMessage;

    public LOGResponse() {
    }

    public LOGResponse(String serverMessage) {
        this.serverMessage = serverMessage;
    }

    public String getServerMessage() {
        return serverMessage;
    }

    public void setServerMessage(String serverMessage) {
        this.serverMessage = serverMessage;
    }
}
