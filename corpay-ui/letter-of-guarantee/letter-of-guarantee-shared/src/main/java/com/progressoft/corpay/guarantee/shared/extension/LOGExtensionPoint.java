package com.progressoft.corpay.guarantee.shared.extension;

import org.dominokit.domino.api.shared.extension.ExtensionPoint;

public interface LOGExtensionPoint extends ExtensionPoint<LOGContext> {
}
