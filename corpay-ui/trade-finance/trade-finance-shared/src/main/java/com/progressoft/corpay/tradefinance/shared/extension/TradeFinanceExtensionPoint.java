package com.progressoft.corpay.tradefinance.shared.extension;

import com.progressoft.corpay.componentcase.shared.extension.AppModuleContext;
import org.dominokit.domino.api.shared.extension.ExtensionPoint;

public interface TradeFinanceExtensionPoint extends ExtensionPoint<TradeFinanceContext>{
}
