package com.progressoft.corpay.tradefinance.shared.request;

import org.dominokit.domino.api.shared.request.RequestBean;

public class TradeFinanceRequest implements RequestBean {

    private String message;

    public TradeFinanceRequest() {
    }

    public TradeFinanceRequest(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
