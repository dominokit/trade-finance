package com.progressoft.corpay.tradefinance.shared.response;

import org.dominokit.domino.api.shared.request.ResponseBean;

public class TradeFinanceResponse implements ResponseBean {

    private String serverMessage;

    public TradeFinanceResponse() {
    }

    public TradeFinanceResponse(String serverMessage) {
        this.serverMessage = serverMessage;
    }

    public String getServerMessage() {
        return serverMessage;
    }

    public void setServerMessage(String serverMessage) {
        this.serverMessage = serverMessage;
    }
}
