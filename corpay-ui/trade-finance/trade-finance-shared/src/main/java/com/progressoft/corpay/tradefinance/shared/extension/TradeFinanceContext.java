package com.progressoft.corpay.tradefinance.shared.extension;

import com.progressoft.corpay.componentcase.shared.extension.AppModuleContext;
import org.dominokit.domino.api.shared.extension.Context;

public interface TradeFinanceContext extends Context {
    String PATH = "Trade finance";

    AppModuleContext getAppModuleContext();
}
