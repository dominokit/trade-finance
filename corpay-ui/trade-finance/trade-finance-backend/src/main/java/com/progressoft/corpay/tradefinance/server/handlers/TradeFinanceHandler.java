package com.progressoft.corpay.tradefinance.server.handlers;

import org.dominokit.domino.api.server.handler.Handler;
import org.dominokit.domino.api.server.handler.RequestHandler;
import org.dominokit.domino.api.server.context.ExecutionContext;
import com.progressoft.corpay.tradefinance.shared.response.TradeFinanceResponse;
import com.progressoft.corpay.tradefinance.shared.request.TradeFinanceRequest;

import java.util.logging.Logger;

@Handler("TradeFinanceRequest")
public class TradeFinanceHandler implements RequestHandler<TradeFinanceRequest, TradeFinanceResponse> {
    private static final Logger LOGGER= Logger.getLogger(TradeFinanceHandler.class.getName());
    @Override
    public void handleRequest(ExecutionContext<TradeFinanceRequest, TradeFinanceResponse> executionContext) {
        LOGGER.info("message recieved from client : "+executionContext.getRequestBean().getMessage());
        executionContext.end(new TradeFinanceResponse("Server message"));
    }
}
