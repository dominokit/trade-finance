package com.progressoft.corpay.tradefinance.server;

import com.google.auto.service.AutoService;
import com.progressoft.corpay.tradefinance.server.handlers.TradeFinanceHandler;
import com.progressoft.corpay.tradefinance.server.handlers.TradeFinanceHandlerEndpointHandler;
import java.util.function.Supplier;
import javax.annotation.Generated;
import org.dominokit.domino.api.server.config.ServerModuleConfiguration;
import org.dominokit.domino.api.server.endpoint.EndpointsRegistry;
import org.dominokit.domino.api.server.handler.HandlerRegistry;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.server.ServerModuleAnnotationProcessor")
@AutoService(ServerModuleConfiguration.class)
public class TradeFinanceServerModule implements ServerModuleConfiguration {
  @Override
  public void registerHandlers(HandlerRegistry registry) {
    registry.registerHandler("TradeFinanceRequest",new TradeFinanceHandler());
  }

  @Override
  public void registerEndpoints(EndpointsRegistry registry) {
    registry.registerEndpoint("TradeFinanceRequest", new Supplier<TradeFinanceHandlerEndpointHandler>() {
      @Override
      public TradeFinanceHandlerEndpointHandler get() {
        return new TradeFinanceHandlerEndpointHandler();
      }
    });
  }
}
