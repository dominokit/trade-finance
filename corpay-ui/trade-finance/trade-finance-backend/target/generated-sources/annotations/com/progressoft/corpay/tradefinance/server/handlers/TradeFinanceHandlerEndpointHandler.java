package com.progressoft.corpay.tradefinance.server.handlers;

import com.progressoft.corpay.tradefinance.shared.request.TradeFinanceRequest;
import com.progressoft.corpay.tradefinance.shared.response.TradeFinanceResponse;
import javax.annotation.Generated;
import org.dominokit.domino.api.server.endpoint.AbstractEndpoint;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.server.EndpointsProcessor")
public class TradeFinanceHandlerEndpointHandler extends AbstractEndpoint<TradeFinanceRequest, TradeFinanceResponse> {
  @Override
  protected TradeFinanceRequest makeNewRequest() {
    return new TradeFinanceRequest();
  }

  @Override
  protected Class<TradeFinanceRequest> getRequestClass() {
    return TradeFinanceRequest.class;
  }
}
