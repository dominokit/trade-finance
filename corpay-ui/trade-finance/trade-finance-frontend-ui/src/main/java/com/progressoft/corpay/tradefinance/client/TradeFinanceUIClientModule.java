package com.progressoft.corpay.tradefinance.client;

import com.google.gwt.core.client.EntryPoint;
import org.dominokit.domino.api.client.ModuleConfigurator;
import org.dominokit.domino.api.client.annotations.ClientModule;
import com.progressoft.corpay.tradefinance.client.ui.views.Bundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.progressoft.corpay.tradefinance.client.ui.views.TradeFinanceBundle;

@ClientModule(name="TradeFinanceUI")
public class TradeFinanceUIClientModule implements EntryPoint {

	private static final Logger LOGGER = LoggerFactory.getLogger(TradeFinanceUIClientModule.class);

	public void onModuleLoad() {
		LOGGER.info("Initializing TradeFinance frontend UI module ...");
		Bundle.INSTANCE.style().ensureInjected();
		new ModuleConfigurator().configureModule(new TradeFinanceUIModuleConfiguration());
	}
}
