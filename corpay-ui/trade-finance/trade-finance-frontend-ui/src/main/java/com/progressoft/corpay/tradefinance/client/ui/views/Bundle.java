package com.progressoft.corpay.tradefinance.client.ui.views;

import com.google.gwt.core.client.GWT;

public class Bundle {

    public static final TradeFinanceBundle INSTANCE= GWT.create(TradeFinanceBundle.class);

    private Bundle() {
    }
}
