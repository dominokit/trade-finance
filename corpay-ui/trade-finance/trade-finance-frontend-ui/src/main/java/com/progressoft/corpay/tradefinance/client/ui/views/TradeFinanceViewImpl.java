package com.progressoft.corpay.tradefinance.client.ui.views;

import com.progressoft.corpay.tradefinance.client.presenters.TradeFinancePresenter;
import com.progressoft.corpay.tradefinance.client.views.TradeFinanceView;
import org.dominokit.domino.api.client.annotations.UiView;
import org.dominokit.domino.api.shared.extension.Content;
import org.dominokit.domino.ui.cards.Card;

@UiView(presentable = TradeFinancePresenter.class)
public class TradeFinanceViewImpl implements TradeFinanceView {

    @Override
    public Content getContent() {
        return () -> Card.create("Trade Finance").asElement();
    }
}