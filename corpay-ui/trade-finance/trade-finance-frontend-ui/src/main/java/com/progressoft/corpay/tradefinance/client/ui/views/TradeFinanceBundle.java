package com.progressoft.corpay.tradefinance.client.ui.views;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;

public interface TradeFinanceBundle extends ClientBundle{

    interface Style extends CssResource {
        String TradeFinance();
    }

    @Source("css/TradeFinance.gss")
    Style style();
}