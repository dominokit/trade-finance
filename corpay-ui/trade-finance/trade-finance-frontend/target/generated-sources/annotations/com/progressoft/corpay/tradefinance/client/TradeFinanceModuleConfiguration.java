package com.progressoft.corpay.tradefinance.client;

import com.progressoft.corpay.componentcase.shared.extension.AppModuleExtensionPoint;
import com.progressoft.corpay.tradefinance.client.contributions.TradeFinancePresenterContributionToAppModuleExtensionPoint;
import com.progressoft.corpay.tradefinance.client.presenters.TradeFinancePresenter;
import com.progressoft.corpay.tradefinance.client.presenters.TradeFinancePresenterCommand;
import com.progressoft.corpay.tradefinance.client.requests.TradeFinanceRequestsFactory;
import com.progressoft.corpay.tradefinance.client.requests.TradeFinanceRequests_requestSender;
import javax.annotation.Generated;
import org.dominokit.domino.api.client.ModuleConfiguration;
import org.dominokit.domino.api.client.extension.ContributionsRegistry;
import org.dominokit.domino.api.client.mvp.PresenterRegistry;
import org.dominokit.domino.api.client.mvp.presenter.LazyPresenterLoader;
import org.dominokit.domino.api.client.mvp.presenter.Presentable;
import org.dominokit.domino.api.client.request.CommandRegistry;
import org.dominokit.domino.api.client.request.LazyRequestRestSenderLoader;
import org.dominokit.domino.api.client.request.RequestRestSender;
import org.dominokit.domino.api.client.request.RequestRestSendersRegistry;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.module.client.ClientModuleAnnotationProcessor")
public class TradeFinanceModuleConfiguration implements ModuleConfiguration {
  @Override
  public void registerPresenters(PresenterRegistry registry) {
    registry.registerPresenter(new LazyPresenterLoader(TradeFinancePresenter.class.getCanonicalName(), TradeFinancePresenter.class.getCanonicalName()) {
      @Override
      protected Presentable make() {
        return new TradeFinancePresenter();
      }
    });
  }

  @Override
  public void registerRequests(CommandRegistry registry) {
    registry.registerCommand(TradeFinancePresenterCommand.class.getCanonicalName(), TradeFinancePresenter.class.getCanonicalName());
  }

  @Override
  public void registerContributions(ContributionsRegistry registry) {
    registry.registerContribution(AppModuleExtensionPoint.class, new TradeFinancePresenterContributionToAppModuleExtensionPoint());
  }

  @Override
  public void registerRequestRestSenders(RequestRestSendersRegistry registry) {
    registry.registerRequestRestSender(TradeFinanceRequestsFactory.TradeFinanceRequests_request.class.getCanonicalName(), new LazyRequestRestSenderLoader() {
      @Override
      protected RequestRestSender make() {
        return new TradeFinanceRequests_requestSender();
      }
    });
  }
}
