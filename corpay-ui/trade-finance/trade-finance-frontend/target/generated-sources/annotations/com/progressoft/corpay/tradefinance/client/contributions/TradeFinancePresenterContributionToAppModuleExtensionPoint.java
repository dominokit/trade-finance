package com.progressoft.corpay.tradefinance.client.contributions;

import com.progressoft.corpay.componentcase.shared.extension.AppModuleExtensionPoint;
import com.progressoft.corpay.tradefinance.client.presenters.TradeFinancePresenterCommand;
import javax.annotation.Generated;
import org.dominokit.domino.api.client.annotations.Contribute;
import org.dominokit.domino.api.shared.extension.Contribution;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.inject.InjectContextProcessor")
@Contribute
public class TradeFinancePresenterContributionToAppModuleExtensionPoint implements Contribution<AppModuleExtensionPoint> {
  @Override
  public void contribute(AppModuleExtensionPoint extensionPoint) {
    new TradeFinancePresenterCommand().onPresenterReady(presenter -> presenter.contributeToAppModule(extensionPoint.context())).send();
  }
}
