package com.progressoft.corpay.tradefinance.client.requests;

import com.progressoft.corpay.tradefinance.shared.request.TradeFinanceRequest;
import com.progressoft.corpay.tradefinance.shared.response.TradeFinanceResponse;
import javax.annotation.Generated;
import org.dominokit.domino.api.client.annotations.Path;
import org.dominokit.domino.api.client.annotations.Request;
import org.dominokit.domino.api.client.request.Response;
import org.dominokit.domino.api.client.request.ServerRequest;

/**
 * This is generated class, please don't modify
 */
@Generated("org.dominokit.domino.apt.client.processors.group.RequestFactoryProcessor")
public class TradeFinanceRequestsFactory implements TradeFinanceRequests {
  public static final TradeFinanceRequestsFactory INSTANCE = new TradeFinanceRequestsFactory();

  @Override
  public Response<TradeFinanceResponse> request(TradeFinanceRequest request) {
    return new TradeFinanceRequests_request(request);
  }

  @Request
  @Path("TradeFinanceRequest")
  public class TradeFinanceRequests_request extends ServerRequest<TradeFinanceRequest, TradeFinanceResponse> {
    TradeFinanceRequests_request(TradeFinanceRequest request) {
      super(request);
    }
  }
}
