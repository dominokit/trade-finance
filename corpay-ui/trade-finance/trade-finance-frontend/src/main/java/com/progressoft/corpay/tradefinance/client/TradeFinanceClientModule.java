package com.progressoft.corpay.tradefinance.client;

import com.google.gwt.core.client.EntryPoint;
import org.dominokit.domino.api.client.ModuleConfigurator;
import org.dominokit.domino.api.client.annotations.ClientModule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ClientModule(name="TradeFinance")
public class TradeFinanceClientModule implements EntryPoint {

	private static final Logger LOGGER = LoggerFactory.getLogger(TradeFinanceClientModule.class);

	public void onModuleLoad() {
		LOGGER.info("Initializing TradeFinance frontend module ...");
		new ModuleConfigurator().configureModule(new TradeFinanceModuleConfiguration());
	}
}
