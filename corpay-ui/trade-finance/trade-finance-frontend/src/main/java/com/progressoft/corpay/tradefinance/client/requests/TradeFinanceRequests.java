package com.progressoft.corpay.tradefinance.client.requests;

import org.dominokit.domino.api.client.annotations.Path;
import org.dominokit.domino.api.client.annotations.RequestFactory;
import org.dominokit.domino.api.client.request.Response;
import com.progressoft.corpay.tradefinance.shared.response.TradeFinanceResponse;
import com.progressoft.corpay.tradefinance.shared.request.TradeFinanceRequest;

@RequestFactory
public interface TradeFinanceRequests {
    @Path("TradeFinanceRequest")
    Response<TradeFinanceResponse> request(TradeFinanceRequest request);
}
