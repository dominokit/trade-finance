package com.progressoft.corpay.tradefinance.client.views;

import com.progressoft.corpay.componentcase.shared.extension.HasContent;
import org.dominokit.domino.api.client.mvp.view.View;

public interface TradeFinanceView extends View, HasContent {
}