package com.progressoft.corpay.tradefinance.client.presenters;

import com.progressoft.corpay.componentcase.shared.extension.AppModuleContext;
import com.progressoft.corpay.componentcase.shared.extension.AppModuleExtensionPoint;
import com.progressoft.corpay.tradefinance.client.views.TradeFinanceView;
import com.progressoft.corpay.tradefinance.shared.extension.TradeFinanceContext;
import com.progressoft.corpay.tradefinance.shared.extension.TradeFinanceExtensionPoint;
import org.dominokit.domino.api.client.annotations.InjectContext;
import org.dominokit.domino.api.client.annotations.Presenter;
import org.dominokit.domino.api.client.mvp.presenter.BaseClientPresenter;

@Presenter
public class TradeFinancePresenter extends BaseClientPresenter<TradeFinanceView> implements TradeFinanceContext {

    public static final String TRADE_FINANCE_HISTORY_TOKEN = "trade-finance";
    private AppModuleContext appModuleContext;

    @InjectContext(extensionPoint = AppModuleExtensionPoint.class)
    public void contributeToAppModule(AppModuleContext context) {
        appModuleContext = context;
        context.addModule(new AppModuleContext.AppModule() {
            @Override
            public String getModuleName() {
                return PATH;
            }

            @Override
            public String getHistoryToken() {
                return TRADE_FINANCE_HISTORY_TOKEN;
            }

            @Override
            public String getPath() {
                return PATH;
            }

            @Override
            public String getModuleIconName() {
                return "credit_card";
            }
        });
        applyContributions(TradeFinanceExtensionPoint.class, () -> this);
    }

    @Override
    public AppModuleContext getAppModuleContext() {
        return appModuleContext;
    }
}