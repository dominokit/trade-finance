package com.progressoft.corpay;

import com.google.gwt.core.client.EntryPoint;
import org.dominokit.domino.api.client.ClientApp;
//import org.realityforge.gwt.keycloak.Keycloak;
//import org.realityforge.gwt.keycloak.KeycloakListenerAdapter;

import java.util.logging.Logger;

public class AppClientModule implements EntryPoint {

    private static final Logger LOGGER = Logger.getLogger(AppClientModule.class.getName());

    public void onModuleLoad() {
//        final Keycloak keycloak = new Keycloak("corpay", "http://localhost:8080/static/keycloak.json");
//        keycloak.setListener(new KeycloakListenerAdapter() {
//            @Override
//            public void onReady(final Keycloak keycloak, final boolean authenticated) {
//                if (authenticated) {
                    ClientApp.make().run();
                    LOGGER.info("corpay-ui Application frontend have been initialized.");
//                } else {
//                    keycloak.login();
//                }
//            }
//        });
//
//        keycloak.init();

    }
}
